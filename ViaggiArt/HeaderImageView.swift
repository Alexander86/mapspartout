//
//  HeaderImage.swift
//  ViaggiArt
//
//  Created by nuccio on 02/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//
import Foundation
import UIKit
import HidingNavigationBar
import CoreLocation
import Contacts
import MapKit
import JASON
import SKPhotoBrowser

open class HeaderImageView: UITableViewCell  {
    
    @IBOutlet weak var galleryButton: UIButton!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    
    @IBOutlet weak var ratio: NSLayoutConstraint!
    @IBOutlet weak var width: NSLayoutConstraint!
    
    
    var parentController: UIViewController?
    var photos: [ SKPhoto ] = [];
    
    open func fillData( _ item: JASON.JSON, view: UIViewController) {
        
        self.title?.text = item["title"].stringValue.uppercased()
        self.subTitle?.text = item["category"]["name"].stringValue
        if(item["type"].stringValue != ""){
            self.categoryImage?.image = UIImage(named: CommonActions.imageCat[item["type"].stringValue]! )
        }
        self.width.constant = frame.width
        self.ratio.constant = frame.width * 2.0 / 3.0;
        if( item["images"]["cover"]["url"].stringValue == ""){
            
            self.mainImage?.load((item["cover"]["url"].stringValue))
            
        }else{
            self.mainImage?.load(item["images"]["cover"]["url"].stringValue)
            
        }
        // }
        
        self.initPhotoBrowser(item);
        
        self.parentController = view;
        self.bringSubview(toFront: galleryButton);
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        self.bringSubview(toFront: galleryButton);
        
    }
    
    @IBAction func eventoComune(_ sender: AnyObject) {
        showPhotoBrowser(parentController!)
    }
    
    public func showPhotoBrowser( _ view: UIViewController )
    {
        
        SKPhotoBrowserOptions.displayToolbar = false                              // all tool bar will be hidden
        SKPhotoBrowserOptions.displayCounterLabel = true                         // counter label will be hidden
        SKPhotoBrowserOptions.displayBackAndForwardButton = true                 // back / forward button will be hidden
        SKPhotoBrowserOptions.displayAction = false                               // action button will be hidden
        SKPhotoBrowserOptions.displayDeleteButton = false                          // delete button will be shown
        SKPhotoBrowserOptions.displayHorizontalScrollIndicator = false            // horizontal scroll bar will be hidden
        SKPhotoBrowserOptions.displayVerticalScrollIndicator = false              // vertical scroll bar will be hidden
        
        
        let browser = SKPhotoBrowser( photos: photos )
        browser.initializePageIndex(0)
        view.present(browser, animated: true, completion: {})
        
    }
    
    func initPhotoBrowser(_ item: JSON) {
        
        let coverUrl = item["images"]["cover"]["url"].stringValue;
        let coverLabel = item["images"]["cover"]["label"].stringValue;
        let coverLicence = item["images"]["cover"]["licence"].stringValue;
        
        
        photos.removeAll()
        
        
        let p = SKPhoto.photoWithImageURL( coverUrl + "&size=big" )
        p.caption = "\(coverLabel)\n[\(coverLicence)]"
        photos.append(p)
        
        item["images"]["others"].forEach { (img) -> () in
            
            let photo = SKPhoto.photoWithImageURL( img["url"].stringValue + "&size=big" )
            photo.caption = "\(img["label"].stringValue)\n[\(img["licence"].stringValue)]"
            
            photos.append(photo)
        }
        
        
        
    }
    
    
    
    
}
