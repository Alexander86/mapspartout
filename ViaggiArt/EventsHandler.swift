//
//  EventsHandler.swift
//  ViaggiArt
//
//  Created by nuccio on 21/02/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//
import UIKit
import Foundation
import SwiftLocation
import JASON

class EventsHandler : EventManager {
    
    fileprivate static let _sharedInstance = EventsHandler()
    
    static var instance:EventsHandler {
        get { return _sharedInstance }
    }
    
    fileprivate override init(){
        super.init();
        
        self.listenTo("showItem") { (item:Any?) -> () in
            let params = item as! NSDictionary
            let id:Int = (params["id"]! as AnyObject).intValue
            let type:String = params["type"]! as! String
            
           
            if( id == -2 ){
                let query:String = params["query"]! as! String
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "SearchView") as! MenuCallableProtocol
                
                DispatchQueue.main.async(execute: {
                    appdelegate.centerNav!.pushViewController(vc as! UIViewController, animated: true)
                    vc.loadData(query)
                })
            }
            else if( id == -1 ){
                //Show list
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.leftViewController?.showList(type)
            }
            else{
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "DetailsView") as! MenuCallableProtocol
                
                DispatchQueue.main.async(execute: {
                    appdelegate.centerNav!.pushViewController(vc as! UIViewController, animated: true)
                    vc.loadData(item)
                })
                
            }
            
            
            
            
            
        }
    }
    
}
