//
//  RelatedTop.swift
//  AltramaApp
//
//  Created by alessandro on 10/05/17.
//  Copyright © 2017 Altrama Italia SRL. All rights reserved.
//

import UIKit
import JASON

public protocol RelatedTopDelegate : NSObjectProtocol {
    
    func showItem( _ type : String , id: Int );
    
}


class RelatedTop: UITableViewCell {
    
    class Item{
        var id:Int = 0
        var title: String?
        var type: String?
        var image: String?
        
        init ( json: JSON ) {
            self.id      = json["id"].intValue
            self.title   = json["title"].stringValue
            self.type    = json["type"].stringValue
            self.image   = json["cover"]["url"].stringValue
        }
    }
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var altezza: NSLayoutConstraint!
    
    var tableView: UITableView? = nil
    var detailView: DetailsView? = nil
    var indexPath: IndexPath? = nil
    
    var firstTime = true;
    
    @IBOutlet weak var title: UILabel!
    var eventHandler: RelatedTopDelegate?;
    var hasMore = true
    var type : String?
    var id : Int?
    var page = 1
    var limit = 5
    var params: NSDictionary?  = nil
    var elements: [Item] = [];
    
    
    open func loadData(type : String?, id: Int?){
        
        self.type = type
        self.id = id
        if(elements.count == 0){
            print("load data first open")
            self.loadRelated({(items) -> Void in
                self.elements.append(contentsOf: items)
                if(self.elements.count > 0){
                    print("num ele \(self.elements.count)")
                    let contentHeight = self.elements.count == 1 ? CGFloat(200) : CGFloat(150)
                    
                    if( (self.detailView?.sectionHeight)! < contentHeight ){
                        self.detailView?.sectionHeight = contentHeight;
                        self.tableView?.reloadData()
                        
                    }
                    
                    self.collectionView.reloadData()
                }else{
                    print("num ele \(self.elements.count)")
                    
                    self.detailView?.sectionHeight = 0;
                    self.collectionView.reloadData()
                    //self.tableView?.reloadData()
                }
            })
        }
        
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let bundle = Bundle(for: type(of: self))
        
        collectionView.register(UINib(nibName: "MultiRelatedTopCellView", bundle: bundle), forCellWithReuseIdentifier: "MyMultiCell")
        collectionView.register(UINib(nibName: "SingleRelatedTopView", bundle: bundle), forCellWithReuseIdentifier: "MySingleCell")
        collectionView.delegate = self
        
        collectionView.dataSource = self
        
        
        
    }
    
    
    func xibSetup() {
        
        
    }
    required public init?(coder aDecoder: NSCoder) {
        //fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
        elements.removeAll()
        xibSetup()
        
        
    }
}

extension RelatedTop :  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    open func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return elements.count
    }
    
    
    
    open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if elements.count == 0{
            return CGSize(width: 0 , height: 0)
        }
        else{
            return CGSize(width: elements.count == 1 ? self.frame.width : 150, height: elements.count == 1 ? 150 : 100);
            
        }
    }
    
    open func collectionView(_ collectionView: UICollectionView,
                             layout collectionViewLayout: UICollectionViewLayout,
                             minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return elements.count == 1 ? 0 : 10
    }
    
    open func collectionView(_ collectionView: UICollectionView,
                             layout collectionViewLayout: UICollectionViewLayout,
                             insetForSectionAt section: Int) -> UIEdgeInsets {
        if(elements.count == 1 ){
            return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        }else{
            return UIEdgeInsets(top: 10,left: 10,bottom: 10,right: 10)
        }
    }
    
    open func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let item = self.elements[(indexPath as NSIndexPath).row]
        let itm : NSDictionary = ["type": item.type! , "id" : item.id, "home" : true];
        EventsHandler.instance.trigger("showItem", information: itm)
        
    }
    
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = elements[(indexPath as NSIndexPath).item];
        
        if( elements.count == 1 ){
            print("single cell")
            let cell:SingleRelatedTopView = collectionView.dequeueReusableCell(withReuseIdentifier: "MySingleCell", for: indexPath) as! SingleRelatedTopView
            
            if(item.id >= 0) {
                cell.image.load(item.image!, placeholder: UIImage(named: "loading"))
            }
            
            
            cell.title.text  = item.title
            cell.image.layer.cornerRadius = 4.0
            cell.image.clipsToBounds = true
            
            
            return cell
        }else {
            print("multi cell")
            let cell:MultiRelatedTopCellView = collectionView.dequeueReusableCell(withReuseIdentifier: "MyMultiCell", for: indexPath) as! MultiRelatedTopCellView
            
            
            
            
            if(item.id >= 0) {
                cell.image.load(item.image!, placeholder: UIImage(named: "loading") )
            }
            cell.title.text  = item.title
            cell.image.layer.cornerRadius = 4.0
            cell.image.clipsToBounds = true
            return cell
            
            
        }
        
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let endScrolling = scrollView.contentOffset.x + scrollView.frame.size.width;
        if (endScrolling >= scrollView.contentSize.width){
            print("loadrelated in automatic mode")
            self.loadRelated({(items) -> Void in
                self.elements.append(contentsOf: items)
                self.collectionView.reloadData()
            })
            
        }
    }
    
    
    
    
    func loadRelated(_ onComplete: @escaping (_ items: [Item])->Void )->Void{
        if( hasMore ){
            
            
            RestApiManager.sharedInstance.getRelatedTop(self.type!, id: self.id!, page: self.page, limit: self.limit, onCompletion: { (json: JSON?) -> Void in
                print("item ricevuti")
                
                var new_items: [Item] = [];
                for item in (json?["data"])! {
                    new_items.append(Item(json: item) )
                }
                
                if( self.page == 1 && json!["meta"]["pagination"]["total"].intValue > 0 ) {
                    self.title.text = json!["meta"]["title"].stringValue
                }
                
                
                self.hasMore = json!["data"].arrayValue.count == self.limit
                if(self.hasMore){
                    self.page = self.page + 1
                }
                onComplete(new_items)
            } )
            
        }
    }
    
    
    
}
