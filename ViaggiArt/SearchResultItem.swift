//
//  contactItem.swift
//  ViaggiArt
//
//  Created by nuccio on 05/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

@IBDesignable open class SearchResultItem: UITableViewCell {

     
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var categoryImage: UIImageView!
   
    @IBOutlet weak var subTitle: UILabel!
    
    @IBOutlet weak var distance: UILabel!
    
    @IBOutlet weak var address: UILabelPad!
    
    
    func loadImage( _ urlImage: String ){
    
        let url = urlImage.replacingOccurrences(of: "list", with: "icon")
        categoryImage.load(url)
    }
    
    
}
