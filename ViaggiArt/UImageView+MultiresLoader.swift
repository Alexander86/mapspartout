//
//  UImageView+MultiresLoader.swift
//  ViaggiArt
//
//  Created by nuccio on 26/06/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation

import UIKit
import SDWebImage

public extension UIImageView {
 
    func load( _ imageUrl: String, placeholder: UIImage? = UIImage(named: "loading")) {
        
        _ = self.contentMode
        self.contentMode = .scaleAspectFill
        self.image = placeholder;
        self.setShowActivityIndicator(true)
        self.setIndicatorStyle(.gray)
        
        self.sd_setImage( with: URL(string:imageUrl),
                                 placeholderImage: image, options: [.progressiveDownload, .delayPlaceholder ])
            
            /*,
                              usingActivityIndicatorStyle: UIActivityIndicatorViewStyle.Gray )*/
        /*
        
        let thumbnail = NSURL(string: imageUrl.stringByReplacingOccurrencesOfString("\(UIScreen.mainScreen().scale)", withString: "0.2"))
        
        self.sd_setImageWithURL(thumbnail!,placeholderImage: placeholder != nil ? placeholder: UIImage(named: "loading"),  completed: {(image: UIImage?, error: NSError?, cacheType: SDImageCacheType!, imageURL: NSURL?) in
            
            self.sd_setImageWithURL( NSURL(string:imageUrl), placeholderImage: image)
        })*/
    }
    
    
   
}
