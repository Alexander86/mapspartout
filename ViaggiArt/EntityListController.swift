//
//  EntityListController.swift
//  ViaggiArt
//
//  Created by nuccio on 12/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import UIKit
import SwiftLocation
import JASON
import SDWebImage
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class EntityListController: HamburgerMenuViewController, UITableViewDelegate, UITableViewDataSource, MenuCallableProtocol
{
    
    class ItemData{
        var id: Int?
        var imageUrl: String?
        var categoryImage: String?
        var categoryName: String?
        var title: String?
        var excerpt: String?
        var address: String?
        var distance : String?
        var type : String?
        
        var date : String?
        var callToActionLabel: String = ""
        var callToActionType: String = ""
        var callToActionValue: String = ""
        var showImage: Bool = true;
        
        @objc func respondToAction() {
            
            
            
            print( callToActionValue )
            
            
            switch self.callToActionType {
                
                
            case "email":
                
                CommonActions.sendEmail( callToActionValue )
                break;
                
                
                
            case "call":
                CommonActions.callNumber(callToActionValue)
                break;
                
                
                
            case "website":
                CommonActions.openURL(callToActionValue)
                break;
            default:
                break;
            }
            
            
        }
        
        init ( json: JSON ) {
            
            self.id      = json["id"].intValue
            self.title   = json["title"].stringValue
            self.excerpt = json["text"]["excerpt"].stringValue
            self.type    = json["type"].stringValue
            
            
            
            if( self.type == CommonActions.NEWSS ){
                
                self.distance = "";
                if( json["elapsed_time"].string != nil ){
                    
                    self.distance = json["elapsed_time"].stringValue
                    
                }                
            }else{
                
                
                let distance = json["distance"].stringValue == "" ? json["distance"].doubleValue == 0.0 ? "" : String(json["distance"].doubleValue) :
                    json["distance"].stringValue
                if distance != "" {
                    self.distance = distance + " " + json["unit"].stringValue
                }
                
            
            }
            
            
            self.address        = json["full_address"].stringValue
            self.categoryName   = json["category"].stringValue
            self.categoryImage  = json["category_image"].stringValue
            self.imageUrl       = json["cover"]["url"].stringValue.replacingOccurrences(of: "2.0", with: "2.0")
            
            
            callToActionLabel = json["call_to_action"]["label"].stringValue;
            callToActionType  = json["call_to_action"]["type"].stringValue;
            callToActionValue = json["call_to_action"]["value"].stringValue;
            
            if(self.type == CommonActions.EVENTS) {
                self.date = json["date"]["full_range_short"].stringValue
            }
            
            self.showImage = json["hide_image"].bool == nil || json["hide_image"].boolValue == false
            
        }
    }
    
    //var type = "culture"
    var page = 1
    var limit = 5
    var link : String?
    var agency = ""
    
    var hasMore = true;
    var url = ""
    var items: [ItemData] = []
    var firstCall = true;
    
    var showHamburger = true
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl?
    var actInd: UIActivityIndicatorView?;
    
    func infinityScrollView() {
        
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
        _ = bounds.size.height
        
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 44))
        //footerView.backgroundColor=UIColor.greenColor()
        
        actInd = UIActivityIndicatorView()
        actInd!.center = CGPoint(x: width/2 , y: 22)
        actInd!.color = UIColor.red
        
        footerView.addSubview(actInd!)
        
        self.tableView.tableFooterView = footerView;
        
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated);
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count == 0 ? 1 : items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if(items.count == 0){
            let cell = UITableViewCell();
            
            cell.textLabel?.text = firstCall ? "Ricerca in corso...".localized : "Non ci sono risultati disponibili".localized
            return cell;
        }else{
        
            firstCall = false;
        
        }
        
        let dataCell = self.items[(indexPath as NSIndexPath).row];
        
        
        if( dataCell.type == CommonActions.MOBILITY) {
            
            var cell: MobilityListItem? = tableView.dequeueReusableCell(withIdentifier: "mobility", for: indexPath) as? MobilityListItem
            
            if( cell == nil){
                
                cell = MobilityListItem(style:UITableViewCellStyle.default, reuseIdentifier:"mobility")
            }
            
            cell?.categoryName.text = dataCell.categoryName
            cell?.address.text = dataCell.address
            cell?.distance.text = dataCell.distance
            cell?.title.text = dataCell.title
            cell!.contentView.setNeedsLayout()
            cell!.contentView.layoutIfNeeded()
            
            return cell!
            
        }
        else
        {
            var cell: ListItemPlace? = tableView.dequeueReusableCell(withIdentifier: "placeItem", for: indexPath) as? ListItemPlace
            
            if( cell == nil){
                
                cell = ListItemPlace(style:UITableViewCellStyle.default, reuseIdentifier:"placeItem")
            }
            
            
            cell!.width.constant = tableView.frame.width
            if( dataCell.showImage ) {
                
                cell!.ratio.constant = tableView.frame.width * 2.0 / 3.0;
                
                cell!.imageMain.load(dataCell.imageUrl! );
            }
            else {
                
                cell!.ratio.constant = 0;
                
            }
            
            //setImageWithURL(url: NSURL(string:dataCell.imageUrl!),usingActivityIndicatorStyle:  )
            cell!.categoryImage.image = UIImage(named: CommonActions.imageCat[dataCell.type!]! )
            cell!.categoryName.text = dataCell.categoryName!
            cell!.title.text = dataCell.title!
            if(dataCell.distance != nil && dataCell.distance! != ""){
            cell!.distance.text = (dataCell.type == CommonActions.NEWS ? "Pubblicato".localized : "Distanza".localized) + " " + dataCell.distance!
            }
            cell!.excerpt.text = dataCell.excerpt!
            
            if dataCell.address! != "" {
                cell!.address.text = dataCell.address!
            }else{
                cell!.address.isHidden = true
                cell!.addressPin.isHidden = true
                cell!.layoutAddress.isHidden = true
            }
            
            
            cell!.callToAction.setTitle(dataCell.callToActionLabel, for: UIControlState())
            
            cell!.callToAction.removeTarget(nil, action: nil, for: .allEvents)
            cell!.callToAction.addTarget(dataCell, action: #selector(dataCell.respondToAction), for: .touchUpInside)
            
            if( dataCell.type == CommonActions.EVENT ){
                
                
                cell!.data.text = dataCell.date
                
            }else{
                
                if( cell!.data != nil){
                    cell!.data.removeFromSuperview()
                }
                
            }
            cell!.contentView.setNeedsLayout()
            cell!.contentView.layoutIfNeeded()
            return cell!
        }
        
        
    }
    
    
    func loadData(_ data: Any?)->Void{
        
        
        let type: [String]? = data as? [String]
        if(type != nil ){
            
            self.link = type![0]
        
            self.limit = 5;

            if( self.link!.contains("mobility")){
                
                self.limit = 20;
                self.agency = type?.count>1 ? type![1] : ""
                
            }
            
            if( type?.count == 4 && type![2] == "hide hamburger" ){
                self.showHamburger = false
            }
            
        }
    }
    
    override func setupHamburgher() {
        if( showHamburger ){
            super.setupHamburgher()
        }
    }
    
    
    func loader(_ onComplete: @escaping (_ items: [ItemData])->Void )->Void{
        if( hasMore ){
            CommonActions.getCurrentLocation { (lat, lng, error) in
                RestApiManager.sharedInstance.getItemsByLocation(self.link!, latitude: lat, longitude:lng, page: self.page, limit: self.limit, options: (self.link!.contains("mobility")) ? ["agency_id" : self.agency] : nil, onCompletion:{ json in
                    
                    var new_items: [ItemData] = [];
                    for item in json["data"] {
                        new_items.append( ItemData(json: item) )
                    }
                    self.page = self.page + 1
                    self.hasMore = json["data"].arrayValue.count == self.limit
                    onComplete(new_items)
                } ) }
            
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NSLog("You selected cell number: \((indexPath as NSIndexPath).row)!")
        
        let dataCell = self.items[(indexPath as NSIndexPath).row];
        let item : NSDictionary = ["type": dataCell.type!, "id" : dataCell.id!];
        
        EventsHandler.instance.trigger("showItem", information: item)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated);
        self.tableView.reloadData();
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,
                                                         selector: #selector(EntityListController.onContentSizeChange(_:)),
                                                         name: NSNotification.Name.UIContentSizeCategoryDidChange,
                                                         object: nil)
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(EntityListController.refreshStream), for: .valueChanged)
        
        self.refreshControl = refresher
        tableView!.addSubview( self.refreshControl!)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.tableView.estimatedRowHeight = 500;
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        let logoImage = UIImage(named: "logo")
        self.navigationItem.titleView = UIImageView(image: logoImage);
        self.navigationController?.navigationBar.tintColor = UIColor.black
        
        
        
        refreshStream()
        
        // Add infinite scroll handler
        infinityScrollView();
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height + 1500;
        if (endScrolling >= scrollView.contentSize.height){
            self.actInd?.startAnimating();
            
            self.loader({(items) -> Void in
                self.items.append(contentsOf: items)
                self.actInd?.stopAnimating();
                self.tableView.reloadData()
            })            
        }
    }

    
    override func viewDidDisappear(_ animated: Bool)  {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    func onContentSizeChange(_ notification: Notification) {
        tableView.reloadData()
    }
    
    func refreshStream() {
        firstCall = true;
        self.page = 1;
        self.hasMore = true;
        self.refreshControl?.beginRefreshing();
        self.loader({ (items) -> Void in
            self.items = items;
            self.refreshControl!.endRefreshing()
            self.tableView.reloadData();
        })
    }
    
}
