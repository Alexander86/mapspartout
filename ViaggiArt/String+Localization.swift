//
//  String+Localization.swift
//  ViaggiArt
//
//  Created by nuccio on 08/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation


public extension String {
    public  var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    public func localizedWithComment(_ comment:String) -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: comment)
    }
}

