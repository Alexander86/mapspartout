//
//  HamburgerMenuViewController.swift
//  ViaggiArt
//
//  Created by nuccio on 30/01/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import UIKit
import JTHamburgerButton
import MMDrawerController
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class HamburgerMenuViewController: UIViewController {
   
    var button: JTHamburgerButton? = nil
    
    internal func checkExplore (){
        
        /*
         let s_button = UIButton()
         s_button.setImage( UIImage(named:"search"), forState: .Normal )
         s_button.frame = CGRectMake(0, 0, 22, 22)
         s_button.addTarget(self, action: #selector(self.startSearch), forControlEvents: UIControlEvents.TouchUpInside)
         
         
         var buttons : [UIBarButtonItem] = [ UIBarButtonItem(customView: s_button) ]
         */
        var buttons : [UIBarButtonItem] = []
        if( CommonActions.isExploreEnabled ){
            
            let button = UIButton()
            button.setImage( UIImage(named:"explore_mode"), for: UIControlState() )
            button.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
            button.addTarget(self, action: #selector(self.setExploreButtonOn), for: UIControlEvents.touchUpInside)
            
            
            buttons.append( UIBarButtonItem(customView: button) )
        }
        
        
        
        self.navigationItem.setRightBarButtonItems(buttons, animated: false);
    }
    
    
    func setupHamburgher () {
        self.button = JTHamburgerButton(frame: CGRect(x: 0, y: 10, width: 32, height: 32));
        self.button!.currentMode = JTHamburgerButtonMode.hamburger;
        self.button!.lineColor = UIColor.black
        self.button!.updateAppearance()
        self.button!.addTarget(self, action: #selector(HamburgerMenuViewController.OnShowMenu(_:)), for: UIControlEvents.touchDown )
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button!);
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let logoImage = UIImage(named: "logo")
        self.navigationItem.titleView = UIImageView(image: logoImage);
        
          navigationController?.hidesBarsOnSwipe = false
         self.navigationController?.navigationBar.tintColor = UIColor.black
        
        setupHamburgher()
        checkExplore ()
    }
    
    func setExploreButtonOn(_ sender: UIButton?){
        
        
        let alert = UIAlertController(title: "ViaggiArt", message: "you are in  explore mode".localized, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Esci".localized, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) in
            
            
            CommonActions.stopExploreMode();
            self.checkExplore();
            
            }
            ))
        
        alert.addAction(UIAlertAction(title: "Continua".localized, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) in
            
            }
            ))
        
        if let topController = UIApplication.topViewController() {
            
            topController.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    func startSearch(){
       
        /*let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate;
        appDelegate.leftViewController?.showSearch();*/
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let currentViewController = mainStoryboard.instantiateViewController(withIdentifier: "SearchView")
        self.navigationController?.pushViewController(currentViewController, animated: true)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func OnShowMenu(_ sender: JTHamburgerButton) {
        
        
        
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate;
        
        
        appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil);
        
        self.toggle(appDelegate);
        
    }
    
    internal func toggle(_ appDelegate: AppDelegate? = nil){
        
        if( button == nil){
            return
        }
        
        var myAppDelegate = appDelegate;
        
        if(myAppDelegate == nil){
            myAppDelegate = UIApplication.shared.delegate as? AppDelegate;
        }
        
        if(  myAppDelegate?.centerContainer!.visibleLeftDrawerWidth > 0){
            button!.setCurrentModeWithAnimation(JTHamburgerButtonMode.arrow)
            
        }
        else {
            button!.setCurrentModeWithAnimation(JTHamburgerButtonMode.hamburger)
        }
    }
    
    override var shouldAutorotate : Bool {
        return false;
    }

}
