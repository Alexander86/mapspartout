//
//  UIButtonClosureAction.swift
//  ViaggiArt
//
//  Created by nuccio on 07/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import UIKit
typealias Closure=(_ sender:UIButton_Closure)->Void

class UIButton_Closure: UIButton {
        var aClosure:Closure?
        
        func actionHandle(controlEvents event :UIControlEvents, ForAction action:Closure?) ->Void{
            
            aClosure = action
            
            self.addTarget(self, action: NSSelectorFromString(self.eventName(event) as String), for: event)
            
        }
        
        
        func eventName(_ event:UIControlEvents) -> NSString{
            switch (event){
            case UIControlEvents.touchCancel: return "TouchCanel";
            case UIControlEvents.touchDown: return "TouchDown";
            case UIControlEvents.touchDownRepeat: return "TouchDownRepeat";
            case UIControlEvents.touchDragEnter: return "TouchDragEnter";
            case UIControlEvents.touchDragExit: return "TouchDragExit";
            case UIControlEvents.touchDragInside: return "TouchDragInside";
            case UIControlEvents.touchDragOutside: return "TouchDragOutside";
            case UIControlEvents.touchUpOutside: return "TouchUpOutside";
            case UIControlEvents.touchUpInside: return "TouchUpInside";
            case UIControlEvents.valueChanged: return "ValueChanged";
            case UIControlEvents.allEditingEvents: return "AllEditingEvents";
            case UIControlEvents.allEvents: return "AllEvents";
            case UIControlEvents.allTouchEvents: return "AllTouchEvents";
            case UIControlEvents.editingChanged: return "EditingChanged";
            case UIControlEvents.editingDidBegin: return "EditingDidBegin";
            case UIControlEvents.editingDidEnd: return "EditingDidEnd";
            case UIControlEvents.editingDidEndOnExit: return "EditingDidEndOnExit";
                
            default : return "description"
            }
        }
        func callBack(_ events:UIControlEvents){
            if (aClosure != nil){
                aClosure!(self)
            }
        }
        func AllTouchEvents(){
            self.callBack(UIControlEvents.allTouchEvents)
        }
        func EditingChanged(){
            self.callBack(UIControlEvents.editingChanged)
        }
        func EditingDidBegin(){
            self.callBack(UIControlEvents.editingDidBegin)
        }
        func EditingDidEnd(){
            self.callBack(UIControlEvents.editingDidEnd)
        }
        func EditingDidEndOnExit(){
            self.callBack(UIControlEvents.editingDidEndOnExit)
        }
        func TouchDragInside(){
            self.callBack(UIControlEvents.touchDragInside)
        }
        func TouchDragOutside(){
            self.callBack(UIControlEvents.touchDragOutside)
        }
        func TouchUpOutside(){
            self.callBack(UIControlEvents.touchUpOutside)
        }
        func ValueChanged(){
            self.callBack(UIControlEvents.valueChanged)
        }
        func AllEditingEvents(){
            self.callBack(UIControlEvents.allEditingEvents)
        }
        func TouchCanel(){
            self.callBack(UIControlEvents.touchCancel)
        }
        func TouchDragExit(){
            self.callBack(UIControlEvents.touchDragExit)
        }
        func TouchDragEnter(){
            self.callBack(UIControlEvents.touchDragEnter)
        }
        func TouchDownRepeat(){
            self.callBack(UIControlEvents.touchDownRepeat)
        }
        func TouchDown(){
            self.callBack(UIControlEvents.touchDown)
        }
        func TouchUpInside(){
            self.callBack(UIControlEvents.touchUpInside)
        }
    
}

