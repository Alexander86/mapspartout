//
//  RelatedTableCell.swift
//  ViaggiArt
//
//  Created by Stefano Vena on 25/01/17.
//  Copyright © 2017 Altrama Italia SRL. All rights reserved.
//

import UIKit

@IBDesignable open class RelatedTableCell: UITableViewCell  {
    
    @IBOutlet weak var imageBig: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var testo: UILabel!
    @IBOutlet weak var distanza: UILabel!
    
    func updateValue( item : EntityListController.ItemData ){
    
        self.imageBig.load( item.imageUrl! )
        self.title.text = item.title!
        self.category.text = item.categoryName!
        self.testo.text = item.excerpt!
        self.distanza.text = item.distance!
    }

}
