//
//  contactItem.swift
//  ViaggiArt
//
//  Created by nuccio on 05/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable open class DetailsItemView: UITableViewCell {

    typealias DetailsClosure = ()-> Void
    var actionFunc: DetailsClosure?
    
    @IBAction func onClick(_ sender: AnyObject) {
        actionFunc!();
    }
    
    @IBOutlet var contactImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var action: UIButton!
   
    
    func actionToCall( _ action: @escaping DetailsClosure ) -> Void{
        self.actionFunc = action;
        self.bringSubview( toFront: self.action );
    }
   
}
