//
//  Region.swift
//  ViaggiArt
//
//  Created by nuccio on 28/04/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation

open class vaRegion {
    
    var latitude:  Double = 0.0
    var longitude: Double = 0.0

    var latitudeDelta:  Double  = 0.0
    var longitudeDelta: Double = 0.0
    
    func contains(_ region: vaRegion) -> Bool {
    
        return self.latitude  + latitudeDelta   > region.latitude  + region.latitudeDelta &&
               self.latitude  - latitudeDelta   < region.latitude  - region.latitudeDelta &&
               self.longitude + longitudeDelta  > region.longitude + region.longitudeDelta &&
               self.longitude - longitudeDelta  < region.longitude - region.longitudeDelta
        
    }
    
    func extend ( _ amount: Double ) -> vaRegion {
        let region = self;
        region.latitudeDelta  = region.latitudeDelta  * amount
        region.longitudeDelta = region.longitudeDelta * amount
        
        return region
    }
    
    
    var minLatitude: Double { get { return self.latitude  - latitudeDelta; } }
    var maxLatitude: Double { get { return self.latitude  + latitudeDelta; } }
    
    var minLongitude: Double { get { return self.longitude  - longitudeDelta; } }
    var maxLongitude: Double { get { return self.longitude  + longitudeDelta; } }
    
}
