//
//  CommonActions.swift
//  ViaggiArt
//
//  Created by nuccio on 06/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import Contacts
import MapKit
import SwiftLocation


open class CommonActions {
    
    static var lastLatitude: Double?
    static var lastLongitude: Double?
    static var isExploreEnabled: Bool = false
    
    open static let PLACES = "places", EVENTS = "events", EATS = "eat", SHOPS = "shop",MOBILITIES = "mobility",STAYS = "stay", NEWSS = "news", TIPS = "tips", OFFERS = "offers", OTHERS1 = "dish", OTHERS2 = "others1";
    open static let MOBILITY = "mobility", PLACE = "place", EVENT = "event", EAT = "eat", SHOP = "shop",STAY = "stay", NEWS = "news", TIP = "tip", OFFER = "offer", OTHER1 = "dish", OTHER2 = "other1";
    
    open static var categories : [String: String] = [PLACES: PLACE, EVENTS: EVENT, EATS: EAT, SHOPS: SHOP, MOBILITIES: MOBILITY, STAYS: STAY, NEWSS: NEWS, TIPS: TIP, OFFERS: OFFER, OTHERS1: OTHER1, OTHERS2: OTHER2]
    
    open static var imageCat = [EVENT : "ic_cosa_fare" , PLACE : "ic_cosa_vedere" , OTHER1 : "ic_piatti" , EAT : "ic_ristoranti" , NEWS : "ic_newsletter"]
    
    open static var imageMenu = [EVENT : "menu_cosa_fare" , PLACE : "menu_cosa_vedere" , OTHER1 : "menu_piatti" , EAT : "menu_ristoranti" , NEWS : "menu_newsletter"]
    
    open static var imageMap = [EVENT : "map_cosa_fare" , PLACE : "map_cosa_vedere" , OTHER1 : "map_piatti" , EAT : "map_ristoranti"]
    
    open static func gotoNavigator( _ lat: Double, lng: Double, address: String, city: String, country: String, zipcode: String){
        
        let addressDict: NSDictionary =
            [CNPostalAddressStreetKey as NSString: address,
             CNPostalAddressCityKey as NSString: city,
             CNPostalAddressStateKey as NSString: country,
             CNPostalAddressPostalCodeKey as NSString: zipcode]
        
        let place = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: lng),addressDictionary: (addressDict as! [String : AnyObject]) )
        
        let mapItem = MKMapItem(placemark: place)
        
        let options = [MKLaunchOptionsDirectionsModeKey:
            MKLaunchOptionsDirectionsModeDriving]
        
        mapItem.openInMaps(launchOptions: options)
        
    }
    
    open static func callNumber(_ phoneNumber:String) {
        // custom whatsapp messsage ok
        /*   if let phoneCallURL:URL = URL(string:"https://api.whatsapp.com/send?phone=+393400850982&text=urlencodedtext" ) {
         let application:UIApplication = UIApplication.shared
         if (application.canOpenURL(phoneCallURL)) {
         application.openURL(phoneCallURL);
         }
         }
         */
        
        // call ok
        if let phoneCallURL:URL = URL(string:"tel://" + phoneNumber.replacingOccurrences(of: " ", with: "")) {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }
        
    }
    
    open static func sendEmail(_ email:String) {
        if let phoneCallURL:URL = URL(string:"mailto://" + email.replacingOccurrences(of: " ", with: "")) {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }
    }
    
    open static func openURL( _ url:String) {
        if let phoneCallURL:URL = URL(string:url) {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }
    }
    
    open static func startExploreMode( _ lat: Double, lng: Double ){
        CommonActions.isExploreEnabled = true;
        CommonActions.lastLatitude = lat
        CommonActions.lastLongitude = lng
        
        //self.navigationController?.popToRootViewControllerAnimated(true)
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        _ = appdelegate.centerNav?.popToRootViewController(animated: true)
        appdelegate.leftViewController?.showHome()
    }
    
    open static func stopExploreMode( ){
        CommonActions.isExploreEnabled = false;
        
        //self.navigationController?.popToRootViewControllerAnimated(true)
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        _ = appdelegate.centerNav?.popToRootViewController(animated: true)
        appdelegate.leftViewController?.showHome()
    }
    
    open static func getCurrentLocation(_ showAlert: Bool? = true, onLocation: @escaping (_ lat:Double, _ lng:Double, _ error: Bool?)-> Void ){
        
        if( CommonActions.isExploreEnabled ){
            
            onLocation(CommonActions.lastLatitude!, CommonActions.lastLongitude!, false)
            
        }else {
            Location.getLocation(accuracy: .any, frequency: .oneShot, timeout: nil, success: { (locRequest, location) -> (Void) in
                DispatchQueue.main.async(execute: {
                    onLocation(location.coordinate.latitude, location.coordinate.longitude, false);
                });            }, error: { (locRequest, location, error) -> (Void) in
                    if( showAlert! ){
                        let alert = UIAlertController(title: "NomeApp".localized, message: "Error on location".localized, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Continua".localized, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) in
                            
                            CommonActions.isExploreEnabled = true;
                            CommonActions.lastLatitude = 39.6488425
                            CommonActions.lastLongitude = 16.4492155
                            DispatchQueue.main.async(execute: {
                                onLocation(CommonActions.lastLatitude!, CommonActions.lastLongitude!, false)})
                        }
                        ))
                        
                        if let topController = UIApplication.topViewController() {
                            
                            topController.present(alert, animated: true, completion: nil)
                        }
                    }
                    else {
                        
                        CommonActions.isExploreEnabled = true;
                        CommonActions.lastLatitude = 39.6488425
                        CommonActions.lastLongitude = 16.4492155
                        DispatchQueue.main.async(execute: {
                            onLocation(CommonActions.lastLatitude!, CommonActions.lastLongitude!, false)
                        })
                        
                    }
                    
            })
            
            
            
        }
    }
    
}
