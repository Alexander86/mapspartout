//
//  NSBundle+Versions.swift
//  ViaggiArt
//
//  Created by nuccio on 26/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//
import Foundation
extension Bundle {
    
    class var applicationVersionNumber: String {
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }
        return "Version Number Not Available"
    }
    
    class var applicationBuildNumber: String {
        if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            return build
        }
        return "Build Number Not Available"
    }
    
}
