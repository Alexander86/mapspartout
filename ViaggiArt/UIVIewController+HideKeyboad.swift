//
//  UIVIewController+HideKeyboad.swift
//  ViaggiArt
//
//  Created by Stefano Vena on 07/01/17.
//  Copyright © 2017 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
