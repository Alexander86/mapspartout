//
//  BannerItemCell.swift
//  ViaggiArt
//
//  Created by nuccio on 02/02/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//
import UIKit

open class BannerItemCell: UICollectionViewCell {

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    var imageView: UIImageView?
    var id = -1;
    
    func setup(_ frame: CGRect){
        self.imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
        
        self.imageView!.image = UIImage(named:"loading");
        self.imageView!.clipsToBounds = true
        self.imageView!.contentMode = .scaleAspectFill
        self.imageView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.addSubview(imageView!)
    }
   
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup(frame)
        
        
    }

   
 
}
