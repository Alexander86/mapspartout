 //
//  AppDelegate.swift
//  ViaggiArt
//
//  Created by nuccio on 16/01/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import UIKit
import MMDrawerController
import SDWebImage
import Firebase
 
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var notifiche: NotifichePush?
    var centerContainer: MMDrawerController?
    var centerNav:UINavigationController?
    var leftViewController : LeftMenu?
    
    let events = EventsHandler.instance;

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        _ = self.window!.rootViewController
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        print("call homeviewcontroller")

        let centerViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        
        LeftMenu.MenuItem.currentViewController = centerViewController;
         print("call leftmenu")
        leftViewController = mainStoryboard.instantiateViewController(withIdentifier: "LeftSideViewController") as? LeftMenu
        
        
        let leftSideNav = UINavigationController(rootViewController: leftViewController!)
              centerNav = UINavigationController(rootViewController: centerViewController)
        
        centerContainer = MMDrawerController(center: centerNav, leftDrawerViewController: leftSideNav)
        
        centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode();
        centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.panningCenterView;
        centerContainer!.showsShadow = true;        
        centerContainer!.showsStatusBarBackgroundView = false;
        centerContainer!.maximumLeftDrawerWidth = 220;
        
        centerContainer?.setGestureCompletionBlock({ (drawerController, gesture) -> Void in           
        
            
            centerViewController.toggle();
        
        
        });
  
    
        // Set navigation bar tint / background colour
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().isOpaque = true
        UINavigationBar.appearance().isTranslucent = false
        
        // Set Navigation bar Title colour
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.black]
        
        // Set navigation bar ItemButton tint colour
        UIBarButtonItem.appearance().tintColor = UIColor.black
        
        // Set Navigation bar background image
 //       let navBgImage:UIImage = UIImage(named: “bg_blog_navbar_reduced.jpg”)!
 //       UINavigationBar.appearance().setBackgroundImage(navBgImage, forBarMetrics: .Default)
        
        //Set navigation bar Back button tint colour
        //UINavigationBar.appearance().tintColor = UIColor.whiteColor()
       
        window!.rootViewController = centerContainer
        window!.makeKeyAndVisible()

        
        SDImageCache.shared().maxCacheAge = 60 * 60 * 12;
        
        self.notifiche = NotifichePush(application: application)
         self.notifiche?.register()
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        return true
    }
    
    
   
    
    
    func closeDrawer() {
        
        self.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion:nil);
        
        
        
    }
    
/*
    func application(application: UIApplication, supportedInterfaceOrientationsForWindow window: UIWindow?) -> UIInterfaceOrientationMask {
        
        return UIInterfaceOrientationMask.Portrait
        
    }
  */  
    
  
    
   
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    /*
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any] ,fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Notifica")
        switch application.applicationState {
            
        case .inactive:
            print("Inactive")
            self.notifiche?.process(userInfo: userInfo)
            UIApplication.shared.applicationIconBadgeNumber = 0
            completionHandler(.newData)
            
        case .background:
            print("Background")
            //Refresh the local model
            completionHandler(.newData)
            
        case .active:
            print("Active")
            //Show an in-app banner
            let banner = Banner(title: "Notifica".localized, subtitle: (userInfo["aps"] as? NSDictionary)?["alert"] as? String, image: UIImage(named: "logo"), backgroundColor: UIColor.white)
            banner.textColor = UIColor.black
            banner.dismissesOnTap = true
            
            banner.show(duration: 3.0)
            
            banner.didTapBlock = {
                self.notifiche?.process(userInfo: userInfo)
                UIApplication.shared.applicationIconBadgeNumber = 0
            }
            
            banner.didDismissBlock = {
                UIApplication.shared.applicationIconBadgeNumber = 0
            }
            
            
            completionHandler(.newData)
        }
    }
    */

}

