//
//  MiniMapView.swift
//  ViaggiArt
//
//  Created by nuccio on 06/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable open class MiniMapView: UITableViewCell  {
    
    @IBOutlet weak var minimap: UIImageView!
    
    @IBOutlet weak var bottone: UIButton!
    @IBAction func onClick(_ sender: AnyObject) {
    
         CommonActions.gotoNavigator(self.lat!, lng: self.lng! , address: self.address!, city: self.city!, country: self.country!, zipcode: self.zipcode!);
    
    }
    var lat: Double?
    var lng: Double?
    var address: String?
    var city: String?
    var country: String?
    var zipcode: String?
    var zoom: Int?;
    
    open func loadMap( _ lat: Double, lng: Double, address: String, city: String, country: String, zipcode: String, zoom : Int ) {
        
        self.lat = lat;
        self.lng = lng;
        self.address = address;
        self.city = city;
        self.country = country;
        self.zipcode = zipcode;
        self.zoom = zoom
        
        
        let googleKey="AIzaSyCr3XzfKJ8lbqnIQdXPAXJI3Z7cctqIFtM";
        let screenSize: CGRect = UIScreen.main.bounds
        
        let w : Int = Int(ceil(screenSize.width))
        let h : Int = Int(ceil(screenSize.width*100.0/350.0))
        let scale: Int = Int(UIScreen.main.scale);
        let url = "http://maps.google.com/maps/api/staticmap?center=\(self.lat!),\(self.lng!)&zoom=\(zoom)&size=\(w)x\(h)&scale=\(scale)&key=\(googleKey)&style=feature%3Apoi%7Cvisibility%3Aoff";
        
        print(url);
        self.bringSubview(toFront: bottone);
        
        minimap?.sd_setImage( with: URL(string:url), placeholderImage: UIImage(named: "staticmap") ) ;
    }
}
