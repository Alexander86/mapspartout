//
//  vaHelpers.swift
//  ViaggiArt
//
//  Created by nuccio on 17/01/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//


import UIKit
import CoreGraphics
import Foundation


open class vaHelper {
    
    
    open static let ViaggiartRedColor = UIColor.fromRGB(0xD01E25);
    open static let ViaggiartBlueColor = UIColor.fromRGB(0x005274);
    open static let VerdeTerre = UIColor.fromRGB(0xA2A237);
    
    
    open static func cropToBounds(_ image: UIImage, width: Double, height: Double) -> UIImage {
        
        let contextImage: UIImage = UIImage(cgImage: image.cgImage!)
        let contextSize: CGSize = contextImage.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
    
    
    
    open static let colors: [ UIColor ] = [
                                              UIColor.fromRGB(0xD41421),
                                              UIColor.fromRGB(0x156A90),
                                              //UIColor.fromRGB(0x2CC75B),
        
                                              UIColor.fromRGB(0x662F1E),
                                              UIColor.fromRGB(0x684834),
                                              UIColor.fromRGB(0xB867C1),
                                              UIColor.fromRGB(0x66A7E4)
                                            ]
    
}

