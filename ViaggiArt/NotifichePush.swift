//
//  NotifichePush.swift
//  ViaggiArt
//
//  Created by Stefano Vena on 29/04/17.
//  Copyright © 2017 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications
import Async
import BRYXBanner

class NotifichePush : NSObject {
    
    let gcmMessageIDKey = "gcm.message_id"
    //Messaging.messaging().subscribe(toTopic: "/topics/pirillo");
    
    init(application: UIApplication) {
        
        super.init()
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // [END register_for_notifications]
        FirebaseApp.configure()
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        
    }
    func register()  {
        Async.main{
            
            Messaging.messaging().subscribe(toTopic: "news");
            
        }
    }
    
    
    
    func unregister() {
        Async.main{
            
            Messaging.messaging().unsubscribe(fromTopic: "news");
            
        }
    }
    
    
    
    func newMessage(userInfo: [AnyHashable: Any]) {
        
        let application = UIApplication.shared
        switch application.applicationState {
            
        case .inactive:
            print("Inactive")
            self.process(userInfo: userInfo)
            UIApplication.shared.applicationIconBadgeNumber = 0
            return
            
        case .background:
            print("Background")
            //Refresh the local model
            return
            
        case .active:
            print("Active")
            
//print(userInfo)
            
            //Show an in-app banner
            let banner = Banner(title: "Notifica".localized, subtitle: (userInfo["aps"] as? NSDictionary)?["alert"] as? String, image: UIImage(named: "logo"), backgroundColor: UIColor.white)
            banner.textColor = UIColor.black
            banner.dismissesOnTap = true
            banner.show()
            
            banner.didTapBlock = {
                self.process(userInfo: userInfo)
                UIApplication.shared.applicationIconBadgeNumber = 0
            }
            
            banner.didDismissBlock = {
                UIApplication.shared.applicationIconBadgeNumber = 0
            }
            
        }
        
        
    }
    
    
    func process(userInfo: [AnyHashable: Any]){
        
        if userInfo["gcm.message_id"] != nil {
            if let id = userInfo["id"] {
                
                if let type = userInfo["type"] {
                    
                   /* let detail : DetailsView = Storyboard.Main.detailsViewScene.viewController() as! DetailsView;
                    detail.id = Int( id as! String )
                    detail.type = (type as! String)
                
            UIApplication.topViewController()?.navigationController?.pushViewController(detail, animated: true )
                */
                  
                    let itm : NSDictionary = ["type": type , "id" : id, "home" : true];
                    EventsHandler.instance.trigger("showItem", information: itm)
                    
                }
                
                
            }
            
            
            
        }
        
    }
}


extension NotifichePush: UIApplicationDelegate{
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        self.process(userInfo: userInfo)
        
        // Print full message.
        print(userInfo)
    }
    
    // [END receive_message]
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the InstanceID token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
        // FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        self.newMessage(userInfo: userInfo)
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
        
        
    }
    
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension NotifichePush : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        self.newMessage(userInfo: userInfo)
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        self.newMessage(userInfo: userInfo)
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
}
// [END ios_10_message_handling]

extension NotifichePush : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    // [END refresh_token]
}
