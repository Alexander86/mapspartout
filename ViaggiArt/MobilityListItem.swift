//
//  MobilityListItem.swift
//  ViaggiArt
//
//  Created by nuccio on 05/06/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit

class MobilityListItem: UITableViewCell {
    
    
    @IBOutlet weak var categoryName: UILabel!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var distance: UILabel!
    
    @IBOutlet weak var address: UILabel!

}