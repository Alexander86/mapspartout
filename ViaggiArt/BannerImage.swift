//
//  BannerImage.swift
//  ViaggiArt
//
//  Created by nuccio on 31/01/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//
import UIKit
import CoreGraphics
import Foundation

open class BannerImage : UIImageView {

    var type : String = "";
    var  id : Int = 0;
    var url : String = "";
    
    
    
    public init(frame: CGRect, type: String, id: Int, url: String, time: Int){
        super.init(frame: frame);
        
        self.type = type;
        self.id = id
        self.url = url
        
        self.contentMode = .scaleAspectFit
        
        self.image = UIImage(named: "loading");
        let delay = 1.5 * Double(time) * Double(NSEC_PER_SEC)
        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time) {
            self.load(url)
        }
       
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
}
