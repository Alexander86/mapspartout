
//
//  DetailView.swift
//  ViaggiArt
//
//  Created by nuccio on 21/02/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit
import HidingNavigationBar
import CoreLocation
import Contacts
import MapKit
import JASON
import EventKit

class unfloatingFooter : UITableView {

     func allowsFooterViewsToFloat() -> Bool {
    
        return false;
        
    }

}
class DetailsView : UIViewController, UITableViewDelegate, UITableViewDataSource, MenuCallableProtocol {
    
    var item: JSON = []
    var refreshControl: UIRefreshControl?
    var actInd: UIActivityIndicatorView?;
    
    var details: [NSMutableDictionary?]  = []
    var eventStore: EKEventStore = EKEventStore()
    var hasCallToAction: Bool = false;
    var shareButton:UIBarButtonItem? = nil;
    
    var type: String?
    var id: Int?
    
    var page = 1
    var limit = 3
    var hasMore = true;
    var relatedTitle: String = "Scopri nei dintorni".localized
    var relatedItems: [EntityListController.ItemData] = []
    
    var descriptionHeight : CGFloat = 0;
    var sectionHeight : CGFloat = 0;
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableBottom: NSLayoutConstraint!
    @IBOutlet weak var callToAction: UIButton_Closure!
    
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated);
        //tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0 ,0);
    }
    
    func shareAction( _ sender: UIBarButtonItem )
    {
        // let textToShare = "Scopri questo luogo su ViaggiArt".localized
        let textToShare = item["text"]["text"].stringValue
        let myWebsite =  item["title"].stringValue
        let objectsToShare = [textToShare, myWebsite] as [Any]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        activityVC.popoverPresentationController?.barButtonItem = sender;
        self.present(activityVC, animated: true, completion: nil)
        
    }
    
    func isLandscapeOrientation() -> Bool {
        return UIInterfaceOrientationIsLandscape(UIApplication.shared.statusBarOrientation)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return details.count + ((relatedItems.count > 0) ? 1 : 0)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        if( indexPath.section < details.count){
            let item = ((details[(indexPath as NSIndexPath).section]!["items"]  as? NSArray) as Array?)![(indexPath as NSIndexPath).row] as? [String]
            
            if( item![0] == "content" ){
                return CGFloat(max(50,descriptionHeight));
            }else if (item![0] == "section"){
                return CGFloat(sectionHeight);
            }
        }
        
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if( section < details.count ){
            return ((details[section]!["items"]  as? NSArray) as Array?)!.count
        }
        return relatedItems.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = (indexPath as NSIndexPath).section
        let row = (indexPath as NSIndexPath).row
        
        if( section < details.count ){
            let item = ((details[section]!["items"]  as? NSArray) as Array?)![row] as? [String]
            //print(item![0])
            switch item![0]  {
            case "header":
                
                let cell:HeaderImageView? = tableView.dequeueReusableCell(withIdentifier: "HeaderImageView", for:indexPath) as? HeaderImageView
                
                cell!.fillData(self.item,view: self);
                
                
                cell!.contentView.setNeedsLayout()
                cell!.contentView.layoutIfNeeded()
                cell!.contentView.tag = (indexPath as NSIndexPath).row;
                return cell!;
            case "minimap":
                
                let cell:MiniMapView? = tableView.dequeueReusableCell(withIdentifier: "MiniMapView", for:indexPath) as? MiniMapView
                
                
                cell!.contentView.tag = (indexPath as NSIndexPath).row;
                cell!.contentView.setNeedsLayout()
                cell!.contentView.layoutIfNeeded()
                
                let lat = self.item["location"]["latitude"].string == nil ?
                    self.item["location"]["latitude"].doubleValue :
                    Double(self.item["location"]["latitude"].stringValue)!
                
                let lng = self.item["location"]["longitude"].string == nil ?
                    self.item["location"]["longitude"].doubleValue :
                    Double(self.item["location"]["longitude"].stringValue)!
                
                
                
                
                cell?.loadMap( lat ,
                               lng: lng,
                               address: self.item["location"]["address"].stringValue,
                               city: self.item["location"]["city"].stringValue,
                               country: self.item["location"]["country"].stringValue,
                               zipcode: self.item["location"]["zipcode"].stringValue,
                               zoom: type == "city" ? 11 : 15)
                
                
                
                return cell!
                
            case "section":
                
                let cell:RelatedTop? = tableView.dequeueReusableCell(withIdentifier: "RelatedTop", for:indexPath) as? RelatedTop
                
                cell!.detailView = self
                cell!.indexPath = indexPath
                cell!.tableView = self.tableView;
                
                cell!.contentView.tag = (indexPath as NSIndexPath).row;
                print("load related top")
                cell?.loadData(type : self.type, id : self.id)
                cell?.isUserInteractionEnabled = true
                
                return cell!
                
                
            case "content":
                
                let cell:ContentView? = tableView.dequeueReusableCell(withIdentifier: "ContentView", for:indexPath) as? ContentView
                
                cell!.detailView = self
                cell!.indexPath = indexPath
                cell!.tableView = self.tableView;
                cell!.contentView.tag = (indexPath as NSIndexPath).row;
                cell!.configure(self.item["text"]["text"].stringValue)
                return cell!;
                
            case "details":
                
                
                let cell:DetailsItemView? = tableView.dequeueReusableCell(withIdentifier: "DetailsItemView", for:indexPath) as? DetailsItemView
                
                switch item![1] {
                case "address":
                    cell?.contactImage.image = UIImage(named: "pin" )
                    cell?.title.text = self.item["location"]["full_address"].stringValue
                    cell?.subtitle.text = "Ottieni le indicazioni stradali".localized
                    cell?.actionToCall( { ()->Void in
                        
                        let lat = self.item["location"]["latitude"].string == nil ?
                            self.item["location"]["latitude"].doubleValue :
                            Double(self.item["location"]["latitude"].stringValue)!
                        
                        let lng = self.item["location"]["longitude"].string == nil ?
                            self.item["location"]["longitude"].doubleValue :
                            Double(self.item["location"]["longitude"].stringValue)!
                        
                        
                        CommonActions.gotoNavigator(
                            lat,
                            lng: lng,
                            address: self.item["location"]["address"].stringValue,
                            city: self.item["location"]["city"].stringValue,
                            country: self.item["location"]["country"].stringValue,
                            zipcode: self.item["location"]["zipcode"].stringValue)
                        
                        }
                    );
                    break;
                case "telephone":
                    cell?.contactImage.image = UIImage(named: "call" )
                    cell?.title.text = self.item["telefono"].stringValue;
                    cell?.subtitle.text = "Contatta telefonicamente".localized
                    
                    
                    cell?.actionToCall(  { ()->Void in
                        
                        
                        CommonActions.callNumber(self.item["telefono"].stringValue)
                        
                    })
                    
                    break;
                    
                case "telephone2":
                    cell?.contactImage.image = UIImage(named: "call" )
                    cell?.title.text = self.item["telefono2"].stringValue;
                    cell?.subtitle.text = "Contatta telefonicamente".localized
                    
                    
                    cell?.actionToCall( {  ()->Void in
                        
                        
                        CommonActions.callNumber(self.item["telefono2"].stringValue)
                        
                    })
                    
                    break;
                case "date-full":
                    cell?.contactImage.image = UIImage(named: "calendar" )
                    cell?.title.text = self.item["date"]["full_range"].stringValue;
                    cell?.subtitle.text = "Aggiungi al calendario".localized
                    
                    
                    cell?.actionToCall( {  ()->Void in
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        
                        // 3
                        let startDate = dateFormatter.date(from: self.item["date"]["from"].stringValue)
                        // 2 hours
                        let endDate = dateFormatter.date(from: self.item["date"]["to"].stringValue)
                        
                        // 4
                        // Create Event
                        let event = EKEvent(eventStore: self.eventStore)
                        event.calendar = self.eventStore.defaultCalendarForNewEvents
                        
                        event.title = self.item["title"].stringValue
                        event.startDate = startDate!
                        event.endDate = endDate!
                        event.location = self.item["location"]["full_address"].stringValue
                        
                        // 5
                        // Save Event in Calendar
                        
                        var event_id = ""
                        do{
                            try self.eventStore.save(event, span: .thisEvent)
                            event_id = event.eventIdentifier
                        }
                        catch let error as NSError {
                            print("json error: \(error.localizedDescription)")
                            let alertController = UIAlertController(title: self.item["title"].stringValue,
                                                                    message:  "Si è verificato un errore. L'evento non è stato aggiunto".localized, preferredStyle: .alert)
                            
                            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                            alertController.addAction(defaultAction)
                            
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                        if(event_id != ""){
                            let alertController = UIAlertController(title: self.item["title"].stringValue,
                                                                    message:  "Evento aggiunto con successo".localized, preferredStyle: .alert)
                            
                            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                            alertController.addAction(defaultAction)
                            
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                    })
                    
                    break;
                    
                    
                    
                case "date":
                    cell?.contactImage.image = UIImage(named: "calendar" )
                    cell?.title.text = self.item["date"]["full_range"].stringValue;
                    cell?.subtitle.text = "Durata dell'evento".localized
                    break;
                    
                    
                case "email":
                    cell?.contactImage.image = UIImage(named: "email" )
                    cell?.title.text = self.item["email"].stringValue;
                    cell?.subtitle.text = "Invia una email".localized
                    
                    
                    cell?.actionToCall( {  ()->Void in
                        
                        CommonActions.sendEmail(self.item["email"].stringValue)
                        
                    })
                    
                    break;
                case "website":
                    cell?.contactImage.image = UIImage(named: "website" )
                    cell?.title.text = self.item["website"].stringValue;
                    cell?.subtitle.text = "Visita il sito web".localized
                    
                    
                    cell?.actionToCall( {  ()->Void in
                        
                        
                        CommonActions.openURL(self.item["website"].stringValue)
                        
                    })
                    
                    
                    break;
                    
                default: break
                    
                }
                
                
                
                
                cell!.contentView.tag = (indexPath as NSIndexPath).row;
                cell!.contentView.setNeedsLayout()
                cell!.contentView.layoutIfNeeded()
                
                return cell!;
                
            default:
                return UITableViewCell();
            }
        }
        
        //relativi
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RelatedTableCell", for:indexPath) as? RelatedTableCell
        
        cell?.updateValue( item: relatedItems[row] )
        
        return cell!
        
    }
    
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {
        
        if( section < details.count ){
            return (details[section]!["name"] as! String);
        }
        
        return relatedTitle
    }
    
    func tableView (_ tableView: UITableView,
                    heightForHeaderInSection section: Int) -> CGFloat
    {
        let title = self.tableView(tableView, titleForHeaderInSection: section)
        if (title == "") {
            return 0.1
        }
        return 32
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 12
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(indexPath)
        let item = self.relatedItems[(indexPath as NSIndexPath).row]
        let itm : NSDictionary = ["type": item.type! , "id" : item.id, "home" : true];
        EventsHandler.instance.trigger("showItem", information: itm)
        
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let title = self.tableView(tableView, titleForHeaderInSection: section)
        
        // Dequeue with the reuse identifier
        let cell = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "DetailsSectionHeader")
        let header = cell as! DetailsSectionHeader
        header.titleLabel.text = title
        
        return cell
        
    }
    
    
    
    func addPrototype( _ name: String ){
        let yourNibName = UINib(nibName: name, bundle: nil)
        tableView.register(yourNibName, forCellReuseIdentifier: name)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addPrototype("HeaderImageView");
        self.addPrototype("MiniMapView");
        self.addPrototype("ContentView");
        self.addPrototype("RelatedTop")
        self.addPrototype("DetailsItemView");
        self.addPrototype("RelatedTableCell");
        
        let nib = UINib(nibName: "DetailsSectionHeader", bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "DetailsSectionHeader")
        
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(DetailsView.refreshStream), for: .valueChanged)
        
        self.refreshControl = refresher
        tableView!.addSubview( self.refreshControl!)
        self.automaticallyAdjustsScrollViewInsets = false
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 130;
        
        let logoImage = UIImage(named: "logo")
        self.navigationItem.titleView = UIImageView(image: logoImage);
        self.navigationController?.navigationBar.tintColor = UIColor.black
        
        self.refreshControl?.beginRefreshing()
        refreshStream()
        infinityScrollView();
    }
    
    
    
    func refreshStream(){
        _ = vaLoadingActivity.showWithDelay("Loading".localized, disableUI: true, seconds:  0.2)
        
        
        self.page = 1
        self.hasMore = true;
        self.relatedTitle = "Scopri nei dintorni".localized
        self.relatedItems.removeAll()
        
        
        
        RestApiManager.sharedInstance.getItem(self.type!, id: self.id!, onCompletion: { (json: JSON?) -> Void in
            
            
            RestApiManager.sharedInstance.getRelatedTop(self.type!, id: self.id!, page: self.page, limit: self.limit, onCompletion: { (jsonRel: JSON?) -> Void in
                
                let numRel = jsonRel?["data"].arrayValue.count
                
                var info: [[String]] = [];
                self.item = json!["data"] as JSON
                if(self.type! == CommonActions.OTHER1 || self.type! == CommonActions.NEWS){
                    self.details = [
                        ["name": "", "items": [["header"]] ],
                        ["name": "", "items": []],
                    ];
                }else{
                    self.details = [
                        ["name": "", "items": [["header"]] ],
                        ["name": "Informazioni".localized, "items": []],
                    ];
                    
                }
                if( self.item["text"]["text"].stringValue.characters.count > 2 ){
                    self.details.append( ["name": "" /*"Descrizione".localized*/, "items": [["content"]] ] )
                }
                
                
                if( self.shareButton == nil && self.item["title"].object != nil) {
                    
                    self.shareButton = UIBarButtonItem(barButtonSystemItem: .action, target: self,  action: #selector(DetailsView.shareAction(_:)) );
                    self.navigationItem.rightBarButtonItem = self.shareButton;
                    
                }
                
                
                if(!self.hasCallToAction){
                    
                    self.hasCallToAction = true;
                    let callToActionLabel = self.item["call_to_action"]["label"].stringValue;
                    if callToActionLabel != "" {
                        self.callToAction.setTitle(callToActionLabel, for: UIControlState());
                        
                        let callToActionType = self.item["call_to_action"]["type"].stringValue;
                        let callToActionValue = self.item["call_to_action"]["value"].stringValue;
                        
                        self.callToAction.actionHandle(controlEvents: .touchUpInside, ForAction: { (sender:UIButton_Closure)->Void in
                            
                            switch callToActionType {
                                
                                
                            case "email":
                                
                                CommonActions.sendEmail( callToActionValue )
                                break;
                                
                                
                                
                            case "call":
                                CommonActions.callNumber(callToActionValue)
                                break;
                                
                                
                                
                            case "website":
                                CommonActions.openURL(callToActionValue)
                                break;
                                
                            case "explore" :
                                
                                let lat = self.item["location"]["latitude"].string == nil ?
                                    self.item["location"]["latitude"].doubleValue :
                                    Double(self.item["location"]["latitude"].stringValue)!
                                
                                let lng = self.item["location"]["longitude"].string == nil ?
                                    self.item["location"]["longitude"].doubleValue :
                                    Double(self.item["location"]["longitude"].stringValue)!
                                
                                CommonActions.startExploreMode(lat, lng: lng)
                                break;
                            default:
                                break;
                            }
                            
                        });
                        
                        
                    }
                    else {
                        
                        self.callToAction.removeFromSuperview();
                        
                        self.tableBottom.constant = 0;
                        self.tableView.updateConstraints()
                    }
                }
                if( self.item["type"].stringValue != "dish" && (self.item["location"][ "latitude"].stringValue != "" || self.item["location"][ "latitude"].doubleValue != 0 )){
                    
                    
                    info.append(["minimap"])
                    info.append(["details", "address" ])
                    /*
                     if( self.item["type"].stringValue == "mobility" &&
                     self.item["category"]["id"].intValue != 97 ){
                     
                     info.append(["details", "showstops" ])
                     
                     }
                     else
                     {
                     info.append(["details", "explore" ])
                     }
                     */
                }
                if( self.item["telefono"].stringValue != ""){
                    
                    info.append(["details", "telephone"])
                    
                }
                if( self.item["telefono2"].stringValue != ""){
                    
                    info.append(["details", "telephone2"])
                }
                if( self.item["email"].stringValue != ""){
                    
                    info.append(["details", "email"])
                }
                if( self.item["type"].stringValue == "event"){
                    let status = EKEventStore.authorizationStatus(for: EKEntityType.event)
                    
                    switch (status) {
                    case EKAuthorizationStatus.notDetermined:
                        // This happens on first-run
                        self.eventStore.requestAccess(to: EKEntityType.event, completion: {
                            (accessGranted: Bool, error: Optional<Error>) in
                            
                            if accessGranted == true {
                                info.append(["details", "date-full"])
                            } else {
                                info.append(["details", "date"])
                            }
                        })
                    case EKAuthorizationStatus.authorized:
                        // Things are in line with being able to show the calendars in the table view
                        info.append(["details", "date-full"])
                    case EKAuthorizationStatus.restricted, EKAuthorizationStatus.denied:
                        // We need to help them give us permission
                        info.append(["details", "date"])
                    }
                }
                if( self.item["website"].stringValue != "" && !self.item["website"].stringValue.contains("booking.com")){
                    info.append(["details", "website"])
                }
                
                if (numRel != nil && numRel! > 0){
                    info.append(["section"])
                }
                if(info.count>0){
                    self.details[1]!.setObject(info, forKey: "items" as NSCopying)
                }else{
                    self.details.remove(at: 1)
                }
                
                
                self.tableView.reloadData()
                self.refreshControl?.endRefreshing()
                _ = vaLoadingActivity.hide()
            } )
            
        })
    }
    
    
    
    var params: NSDictionary?  = nil
    func loadData(_ data: Any?) {
        
        self.params =  data as? NSDictionary
        self.type = self.params!["type"] as? String
        self.id = (self.params!["id"]! as AnyObject).intValue
        
    }
    
}


//Caricamento relativi
extension DetailsView {
    
    func infinityScrollView() {
        
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
        _ = bounds.size.height
        
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 44))
        //footerView.backgroundColor=UIColor.greenColor()
        
        actInd = UIActivityIndicatorView()
        actInd!.center = CGPoint(x: width/2 , y: 22)
        actInd!.color = UIColor.red
        
        footerView.addSubview(actInd!)
        
        self.tableView.tableFooterView = footerView;
        
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height + 1500;
        if (endScrolling >= scrollView.contentSize.height){
            self.actInd?.startAnimating();
            if(hasMore){
                self.loadRelated({(items) -> Void in
                    self.relatedItems.append(contentsOf: items)
                    self.actInd?.stopAnimating();
                    self.tableView.reloadData()
                })}else{
                self.actInd?.stopAnimating();
                self.tableView.reloadData()
            }
        }
    }
    
    
    
    func loadRelated(_ onComplete: @escaping (_ items: [EntityListController.ItemData])->Void )->Void{
        
        
        
        RestApiManager.sharedInstance.getRelated(self.type!, id: self.id!, page: self.page, limit:self.limit, onCompletion: { (json: JSON?) -> Void in
            
            var new_items: [EntityListController.ItemData] = [];
            for item in (json?["data"])! {
                new_items.append(EntityListController.ItemData(json: item) )
            }
            
            if( self.page == 1 ) {
                self.relatedTitle = json!["meta"]["title"].stringValue
            }
            
            
            self.page = self.page + 1
            self.hasMore = json!["data"].arrayValue.count == self.limit
            
            
            onComplete(new_items)
        } )
        
        
    }
    
    
}






