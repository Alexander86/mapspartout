//
//  ListItemPlace.swift
//  ViaggiArt
//
//  Created by nuccio on 14/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit

class ListItemPlace: UITableViewCell {
    
    @IBOutlet weak var categoryImage: UIImageView!
    
    @IBOutlet weak var categoryName: UILabel!
    
    @IBOutlet weak var title: UILabel!
   
    @IBOutlet weak var excerpt: UILabel!
    
    @IBOutlet weak var imageMain: UIImageView!
    
    @IBOutlet weak var distance: UILabel!
    
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var addressPin: UIImageView!
    @IBOutlet weak var callToAction: UIButton!
    
    @IBOutlet weak var layoutAddress: UIStackView!
    @IBOutlet weak var data: UILabel!
    
    @IBOutlet weak var width: NSLayoutConstraint!
    @IBOutlet weak var ratio: NSLayoutConstraint!
}
