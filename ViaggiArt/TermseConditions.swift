//
//  TermseConditions.swift
//  ViaggiArt
//
//  Created by nuccio on 20/06/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

//
//  EntityListController.swift
//  ViaggiArt
//
//  Created by nuccio on 12/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import UIKit
import JASON
import SDWebImage
import JTHamburgerButton
import MMDrawerController

class TermseConditions: UIViewController, MenuCallableProtocol, UIWebViewDelegate{
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logoImage = UIImage(named: "logo")
        self.navigationItem.titleView = UIImageView(image: logoImage);
        self.navigationController?.navigationBar.tintColor = UIColor.black
        
        webView.delegate = self
       
        self.title = "Terms & Conditions".localized
        
        //load a file
        let testHTML = Bundle.main.path(forResource: "terms", ofType: "html")
        
        do {
            let contents = try NSString(contentsOfFile: testHTML!, encoding: String.Encoding.utf8.rawValue)
            let baseUrl = URL(fileURLWithPath: testHTML!) //for load css file
            webView.loadHTMLString(contents as String, baseURL: baseUrl)
        }
        catch {
            
        }
           }
    
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked {
            UIApplication.shared.openURL(request.url!)
            return false
        }
        return true
    }
    
    func loadData(_ data: Any?) {
    }
    
    
}
