
//
//  DetailView.swift
//  ViaggiArt
//
//  Created by nuccio on 21/02/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit
import HidingNavigationBar
import CoreLocation
import Contacts
import MapKit
import JASON
import EventKit

class MobilityView : UITableViewController, MenuCallableProtocol {
    var item: JSON = []
    var isRealtime: Bool = false
    var details: [NSMutableDictionary?]  = [
        ["name": "", "items": [["header"]] ],
        ["name": "Informazioni".localized, "items": []],
        ["name": "Tabellone".localized, "items": [] ]
    ];
    
    func addPrototype( _ name: String ){
        let yourNibName = UINib(nibName: name, bundle: nil)
        tableView.register(yourNibName, forCellReuseIdentifier: name)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addPrototype("HeaderImageView");
        self.addPrototype("MiniMapView");
        self.addPrototype("ContentView");
        self.addPrototype("DetailsItemView");
        
        let nib = UINib(nibName: "DetailsSectionHeader", bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "DetailsSectionHeader")
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 180;
        
        let logoImage = UIImage(named: "logo")
        self.navigationItem.titleView = UIImageView(image: logoImage);
        self.navigationController?.navigationBar.tintColor = UIColor.black
        
        var info: [[String]] = [];
        
        if(self.item["location"][ "latitude"].stringValue != "" || self.item["location"][ "latitude"].doubleValue != 0 ){
            
            info.append(["minimap"])
            info.append(["details", "address" ])
        }
        
        if( self.item["telefono"].stringValue != ""){
            info.append(["details", "telephone"])
        }
        
        details[1]!.setObject(info, forKey: "items" as NSCopying)
    
        
        var stops: [[String]] = [];
        
        for _ in self.item["stops"] {
        
            stops.append(["stop"]);
        
        }
        
        if( stops.count == 0){
            stops.append(["nostop"]);
        }
        
        details[2]!.setObject(stops, forKey: "items" as NSCopying)
        
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0 ,0);
        isRealtime = self.item["realtime"].boolValue;
    }

    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return details.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return
             ((details[section]!["items"]  as? NSArray) as Array?)!.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = ((details[(indexPath as NSIndexPath).section]!["items"]  as? NSArray) as Array?)![(indexPath as NSIndexPath).row] as? [String]
        
        switch item![0]  {
        case "header":
            
            let cell:HeaderImageView? = tableView.dequeueReusableCell(withIdentifier: "HeaderImageView", for:indexPath) as? HeaderImageView
            
            cell!.fillData(self.item,view: self);
            
            
            cell!.contentView.setNeedsLayout()
            cell!.contentView.layoutIfNeeded()
            cell!.contentView.tag = (indexPath as NSIndexPath).row;
            return cell!;
            
        case "nostop" :
        
            let cell = UITableViewCell();
            cell.textLabel?.text = isRealtime ? "Dati non disponibili".localized : "Nessuna partenza nelle prossime due ore".localized
            return cell;
            
        case "stop" :
            
             let cell:OrarioListItem? = tableView.dequeueReusableCell(withIdentifier: "OrarioListItem", for:indexPath) as? OrarioListItem
             
             let item = self.item["stops"][(indexPath as NSIndexPath).row];
             
             cell?.short_name.text = item["route_short_name"].stringValue
             cell?.short_name.layer.borderWidth = 2.0
             cell?.short_name.layer.cornerRadius = 8
             cell?.short_name.layer.borderColor = UIColor.fromHex(item["route_color"].stringValue).cgColor
             cell?.short_name.layer.masksToBounds = true
             cell?.short_name.textColor = UIColor.black
             
             
             cell?.name.text = item["trip_headsign"].stringValue
             cell?.time.text = item["departure_time"].stringValue
             
             return cell!;
        case "minimap":
            
            let cell:MiniMapView? = tableView.dequeueReusableCell(withIdentifier: "MiniMapView", for:indexPath) as? MiniMapView
            
            
            cell!.contentView.tag = (indexPath as NSIndexPath).row;
            cell!.contentView.setNeedsLayout()
            cell!.contentView.layoutIfNeeded()
            
            let lat = self.item["location"]["latitude"].string == nil ?
                self.item["location"]["latitude"].doubleValue :
                Double(self.item["location"]["latitude"].stringValue)!
            
            let lng = self.item["location"]["longitude"].string == nil ?
                self.item["location"]["longitude"].doubleValue :
                Double(self.item["location"]["longitude"].stringValue)!
            
            
            
            
            cell?.loadMap( lat ,
                           lng: lng,
                           address: self.item["location"]["address"].stringValue,
                           city: self.item["location"]["city"].stringValue,
                           country: self.item["location"]["country"].stringValue,
                           zipcode: self.item["location"]["zipcode"].stringValue,
                           zoom:15)
            
            
            
            return cell!
            
        case "content":
            
            let cell:ContentView? = tableView.dequeueReusableCell(withIdentifier: "ContentView", for:indexPath) as? ContentView
            
            cell!.configure(self.item["text"]["text"].stringValue)
            
            cell!.contentView.tag = (indexPath as NSIndexPath).row;
            cell!.contentView.setNeedsLayout()
            cell!.contentView.layoutIfNeeded()
            
            return cell!;
            
            
            
        case "details":
            
            
            let cell:DetailsItemView? = tableView.dequeueReusableCell(withIdentifier: "DetailsItemView", for:indexPath) as? DetailsItemView
            
            switch item![1] {
            case "address":
                cell?.contactImage.image = UIImage(named: "pin" )
                cell?.title.text = self.item["location"]["full_address"].stringValue
                cell?.subtitle.text = "Ottieni le indicazioni stradali".localized
                cell?.actionToCall( { ()->Void in
                    
                    let lat = self.item["location"]["latitude"].string == nil ?
                        self.item["location"]["latitude"].doubleValue :
                        Double(self.item["location"]["latitude"].stringValue)!
                    
                    let lng = self.item["location"]["longitude"].string == nil ?
                        self.item["location"]["longitude"].doubleValue :
                        Double(self.item["location"]["longitude"].stringValue)!
                    
                    
                    CommonActions.gotoNavigator(
                        lat,
                        lng: lng,
                        address: self.item["location"]["address"].stringValue,
                        city: self.item["location"]["city"].stringValue,
                        country: self.item["location"]["country"].stringValue,
                        zipcode: self.item["location"]["zipcode"].stringValue)
                    
                    }
                );
                break;
            case "telephone":
                cell?.contactImage.image = UIImage(named: "call" )
                cell?.title.text = self.item["telefono"].stringValue;
                cell?.subtitle.text = "Contatta telefonicamente".localized
                
                
                cell?.actionToCall(  { ()->Void in
                    
                    
                    CommonActions.callNumber(self.item["telefono"].stringValue)
                    
                })
                
                break;
                
            case "telephone2":
                cell?.contactImage.image = UIImage(named: "call" )
                cell?.title.text = self.item["telefono2"].stringValue;
                cell?.subtitle.text = "Contatta telefonicamente".localized
                
                
                cell?.actionToCall( {  ()->Void in
                    
                    
                    CommonActions.callNumber(self.item["telefono2"].stringValue)
                    
                })
                
                break;
                
            default: break
                
            }
            
            
            
            
            cell!.contentView.tag = (indexPath as NSIndexPath).row;
            cell!.contentView.setNeedsLayout()
            cell!.contentView.layoutIfNeeded()
            
            return cell!;
            
        default:
            return UITableViewCell();
        }
        
    }
    
    override func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {
        return (details[section]!["name"] as! String);
    }
    
    override func tableView (_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        
        let title = self.tableView(tableView, titleForHeaderInSection: section)
        if (title == "") {
            return 0.0
        }
        return 32.0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let listItem = (((details[(indexPath as NSIndexPath).section]!["items"]  as? NSArray) as Array?)![(indexPath as NSIndexPath).row] as? [String])
        
        
        if( listItem != nil && listItem![0] == "stop" ){
            let item = self.item["stops"][(indexPath as NSIndexPath).row];
               _ = vaLoadingActivity.show( "Loading".localized , disableUI: true )
                RestApiManager.sharedInstance.getTrip(item["trip_id"].stringValue, stop_id:self.item["stop_id"].stringValue, onCompletion: { (json) in
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let view  = mainStoryboard.instantiateViewController(withIdentifier: "MobilityStopList") as? MenuCallableProtocol;
                self.navigationController?.pushViewController(view as! UIViewController, animated: true)
                view?.loadData(json["data"]);
               _ = vaLoadingActivity.hide();
            })
        }        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let title = self.tableView(tableView, titleForHeaderInSection: section)
        
        
        // Dequeue with the reuse identifier
        let cell = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "DetailsSectionHeader")
        let header = cell as! DetailsSectionHeader
        header.titleLabel.text = title
        
        return cell
    }
    
    func loadData(_ data: Any?) {
        self.item = data as! JSON;
    }
    
}
