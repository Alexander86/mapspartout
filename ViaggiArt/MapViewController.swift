//
//  MapViewController.swift
//  ViaggiArt
//
//  Created by nuccio on 30/01/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import UIKit
import MapKit
import FBAnnotationClusteringSwift
import JASON
import SDWebImage

class MapViewController: HamburgerMenuViewController, CLLocationManagerDelegate, MenuCallableProtocol {
    
    @IBOutlet weak var menuholder: UIView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var toolbar: UIToolbar!
    
    @IBOutlet weak var searchbar: UISearchBar!
    
    @IBOutlet weak var filterButton: UIView!
    let clusteringManager = FBClusteringManager()
    var currentRegion = vaRegion()
    
    var downMenuButton: DWBubbleMenuButton!
    
  /*  var filters = [ true  /*culture*/,
                    true  /*events*/,
                    false /*stay*/,
                    false /*eat*/,
                    false /*shop*/,
                    false /*mobility*/]
 
 */
    
    var filters = [ true  /*culture*/,
        true  /*events*/,
        false /*eat*/,
        ]
    
    let selectedColor = UIColor.white
    let unselectedColor = UIColor.gray
    var isFiltersModified = true;
    var waitForFirstLocation = true;
    
    var debounceTimer: Timer?
    var searchDebouncer : DeBouncer = DeBouncer()
    var hideDebouncer : DeBouncer = DeBouncer()
    func loadData(_ data: Any?) {
        
        
    }
    
    
    func createHomeButtonView() -> UIImageView {
        
        let label = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: 44.0, height: 44.0))
            label.image = UIImage(named: "filter")
            label.image = label.image?.withRenderingMode(.alwaysTemplate)
            label.tintColor = UIColor.black
            label.contentMode = .center
       /*
            label.layer.borderWidth = 2
            //label.layer.cornerRadius = 2.0
            label.layer.borderColor = UIColor.fromHex("D32900").darker().cgColor
            // label.textColor = UIColor.whiteColor()
            //label.textAlignment = NSTextAlignment.Center
            label.layer.cornerRadius = label.frame.size.height / 2.0;
            label.backgroundColor = UIColor.fromHex("D32900")
            label.clipsToBounds = true;
 */
        
        return label;
    }
    
    func createDemoButtonArray() -> [UIButton] {
        var buttons:[UIButton]=[]
        var i = 0
       // for str in ["see","play","stay","eat","shop","mobility"] {
        for str in ["see","play","eat"] {
            let button:UIButton = UIButton(type: UIButtonType.system)
            
            button.setImage(UIImage(named: str), for: UIControlState())
            button.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
            button.tintColor = filters[i] ? vaHelper.colors[i] : UIColor.white //? selectedColor: unselectedColor
            
            button.frame = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0);
            button.layer.cornerRadius = 4 //button.frame.size.height / 2.0;
            button.layer.borderWidth = 1
            button.layer.borderColor = button.tintColor.cgColor
            button.backgroundColor = (filters[i] ? UIColor.white : UIColor.gray)
            
            //button.backgroundColor = filters[i] ? vaHelper.colors[i] : vaHelper.colors[i].darker()
            button.layer.borderColor = button.backgroundColor?.darker().cgColor

            //UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.8)
            button.clipsToBounds = true;
            i += 1
            button.tag = i;
            button.addTarget(self, action: #selector(MapViewController.buttonTap(_:)), for: UIControlEvents.touchUpInside)
            
            buttons.append(button)
            
        }
        return buttons
        
    }
    
    func createButtonWithName(_ imageName:NSString) -> UIButton {
        let button = UIButton()
        
        button.setImage(UIImage(named: imageName as String), for: UIControlState())
        button.sizeToFit()
        button.addTarget(self, action: #selector(MapViewController.buttonTap(_:)), for: UIControlEvents.touchUpInside)
        
        return button
        
    }
    
    
    func buttonTap(_ sender:UIButton){
        
        let enabling = sender.tintColor == UIColor.white
        
        sender.backgroundColor = (enabling ? UIColor.white : UIColor.gray)
        sender.tintColor = enabling ? vaHelper.colors[sender.tag-1] : UIColor.white
        
        filters[sender.tag-1] = !filters[sender.tag-1]
        
        isFiltersModified = true;
        
        if let timer = debounceTimer {
                timer.invalidate()
        }
        
        debounceTimer = Timer(timeInterval: 0.7, target: self, selector: #selector(MapViewController.updateMap), userInfo: nil, repeats: false)
            RunLoop.current.add(debounceTimer!, forMode: RunLoopMode(rawValue: "NSDefaultRunLoopMode"))
    
    }
    
   
    func updateMap() {
    
        self.regionChanged(true)
    
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
    
        
        super.viewDidAppear(animated)
        
        self.mapView.showsPointsOfInterest = false
        self.mapView.showsUserLocation = true;
        self.searchbar.delegate = self
        self.searchbar.placeholder = "Inserisci un indirizzo".localized
        self.searchbar.showsCancelButton = false
        self.mapView.tintColor = vaHelper.ViaggiartRedColor
        
        self.downMenuButton = DWBubbleMenuButton(frame: filterButton.frame, expansionDirection: .directionDown)
        
        // Create down menu button
        let homeLabel = self.createHomeButtonView()
        
        downMenuButton.homeButtonView = homeLabel;
        downMenuButton.collapseAfterSelection = false;
        downMenuButton.delegate = self;
        downMenuButton.addButtons(self.createDemoButtonArray())
        self.view.addSubview(downMenuButton)
      
        waitForFirstLocation = true;
        _ = vaLoadingActivity.show("Loading".localized, disableUI: true)
        CommonActions.getCurrentLocation(false) { (lat, lng, error) in
             _ = vaLoadingActivity.hide();
             self.waitForFirstLocation = false;
             self.mapView.setCenterCoordinate( CLLocationCoordinate2D(latitude: lat, longitude: lng), zoomLevel: 16, animated: true)
            
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Map".localized;
        self.navigationItem.title = self.title
        self.navigationController?.navigationBar.tintColor = UIColor.black
        
        clusteringManager.delegate = self;
        self.hideKeyboardWhenTappedAround()
    }
    
    func onClickedToolbeltButton( _ sender: UIBarButtonItem){
    
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension MapViewController: DWBubbleMenuViewDelegate {

    func bubbleMenuButtonWillExpand(_ expandableView:DWBubbleMenuButton){}
    func bubbleMenuButtonDidExpand(_ expandableView:DWBubbleMenuButton){
    
        isFiltersModified = false;
        
        
    
    }
    func bubbleMenuButtonWillCollapse(_ expandableView:DWBubbleMenuButton){
    
    
    }
    func bubbleMenuButtonDidCollapse(_ expandableView:DWBubbleMenuButton){
    
        if(isFiltersModified) {
            //self.regionChanged(true)
        }
    
    }

}

extension MapViewController : FBClusteringManagerDelegate {
    public func cellSizeFactor(forCoordinator coordinator: FBClusteringManager) -> CGFloat {
         return 0.0
    }
}

extension MapViewController : MKMapViewDelegate {
    
    
    
    func callApi( _ byRegion:vaRegion, onCompletion: @escaping (_ json: JSON) -> Void ){
       // let apis:[String] = ["places", "events","stay","eat","shop","mobility"]
        let apis:[String] = [CommonActions.PLACES, CommonActions.EVENTS, CommonActions.EATS]
        
        for i in 0 ..< apis.count{
            
            
            if(filters[i]){
                RestApiManager.sharedInstance.getItems(apis[i], byRegion: byRegion, onCompletion: onCompletion);
            }
        
        }
    
    }
    
    
    func regionChanged(_ forceReload: Bool = false) {
    
        
        
        if( waitForFirstLocation ){
            return
        }
        
        let r : vaRegion = mapView.getRegion()
        
        
        
        DispatchQueue.global(qos: .userInitiated).async {
        if forceReload || ( self.currentRegion.contains(r) == false) {
            
            DispatchQueue.main.async{
                self.clusteringManager.removeAll();
                self.mapView.removeAnnotations( self.mapView.annotations )
            }
            
            self.currentRegion = r.extend(1.3);
            
            var sets = 0;
            
            for filter in self.filters {
                if(filter) {
                    sets += 1
                }
            }
            
            if( sets > 0 ) {
            
            _ = vaLoadingActivity.show("Loading".localized, disableUI: true)
            self.callApi(self.currentRegion, onCompletion: { (data: JSON) -> Void in
                let places = data["data"];
                var array:[FBAnnotation] = []
                
                places.forEach({ (item:JSON) -> () in
                    
                    
                    let a:ViaggiArtMapPin = ViaggiArtMapPin()
                    
                    var lat = Double(item["latitude"].stringValue)
                    var lng = Double(item["longitude"].stringValue)
                    
                    if(lat == nil){
                        lat = item["latitude"].double
                        lng = item["longitude"].double
                    }
                    if(lat != nil && lng != nil){
                        a.title = item["title"].string;
                        a.coordinate = CLLocationCoordinate2DMake( lat!, lng!)
                        a.address = item["full_address"].stringValue
                        a.subtitle = item["category"].stringValue
                       // a.image = item["category_pin"].stringValue
                        a.cover = item["cover"]["url"].stringValue
                        a.id = item["id"].intValue
                        a.type = item["type"].stringValue
                    
                        array.append(a);
                  }
                })
                
                self.clusteringManager.add(annotations: array);
                
                
                DispatchQueue.main.async{
                    
                    let mapBoundsWidth = Double(self.mapView.bounds.size.width)
                    
                    let mapRectWidth:Double = self.mapView.visibleMapRect.size.width
                    
                    let scale:Double = mapBoundsWidth / mapRectWidth
                    
                    let annotationArray = self.clusteringManager.clusteredAnnotations(withinMapRect: self.mapView.visibleMapRect, zoomScale:scale)
                    
                    self.clusteringManager.display(annotations: annotationArray, onMapView:self.mapView)
                    
                    let pages = data["meta"]["pagination"]["total_pages"].intValue
                    let page  = data["meta"]["pagination"]["current_page"].intValue
                    
                    print("\(page) di \(pages)")
                    
                    if( (pages == 0) || ( page == pages) ){
                        sets = sets-1;
                        if( sets <= 0){
                           _ =  vaLoadingActivity.hide()
                        }
                    }
                    
                }
                // }
            });
                
            }
            else {
            
                DispatchQueue.main.async{
                    
                    let mapBoundsWidth = Double(self.mapView.bounds.size.width)
                    
                    let mapRectWidth:Double = self.mapView.visibleMapRect.size.width
                    
                    let scale:Double = mapBoundsWidth / mapRectWidth
                    
                    let annotationArray = self.clusteringManager.clusteredAnnotations(withinMapRect: self.mapView.visibleMapRect, zoomScale:scale)
                    
                    self.clusteringManager.display(annotations: annotationArray, onMapView:self.mapView)
                    
                }

            
            }
        }
        else {
            DispatchQueue.main.async{
                
                let mapBoundsWidth = Double(self.mapView.bounds.size.width)
                
                let mapRectWidth:Double = self.mapView.visibleMapRect.size.width
                
                let scale:Double = mapBoundsWidth / mapRectWidth
                
                let annotationArray = self.clusteringManager.clusteredAnnotations(withinMapRect: self.mapView.visibleMapRect, zoomScale: scale)
                
                self.clusteringManager.display(annotations: annotationArray, onMapView:self.mapView)
                
            }
        }
        }
    
    
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool){
       
        /*
        if( self.mapView.getZoom() < 16 ){
        
            self.mapView.setCenterCoordinate(mapView.camera.centerCoordinate, zoomLevel: 16, animated: true)
        }
        else {
            regionChanged()
       }*/
        
        var region = self.mapView.region;
        let span = self.mapView.region.span;
        let rate = span.longitudeDelta / span.latitudeDelta
        if( span.latitudeDelta > 0.05){

            
            region.span.latitudeDelta = 0.05
            region.span.longitudeDelta = 0.05*rate
            
            self.mapView.setRegion( region, animated: false);
        }
        else{
          
          regionChanged()
        }
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        var reuseId = ""
        
        if annotation.isKind(of: FBAnnotationCluster.self) {
            
            reuseId = "Cluster"
            var clusterView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
            clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: reuseId)
            
            return clusterView
            
        }
        else if annotation is ViaggiArtMapPin {
        
            let item = annotation as! ViaggiArtMapPin
            var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: "pin")
            reuseId = "Pin" + item.type!
            if pinView == nil {                                
                pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                pinView!.canShowCallout = true
               // pinView!.image = RestApiManager.sharedInstance.loadImage(item.image!)
                pinView!.image = UIImage(named: CommonActions.imageMap[item.type!]! )
                pinView!.frame = CGRect(x: 0,y: 0,width: 30,height: 36)
                
                
                // Add image to left callout
                let mugIconView = UIImageView(frame: CGRect(x: 0, y: 0, width: 75, height: 50));
                    mugIconView.contentMode = .scaleAspectFit
                
                
                
                pinView!.leftCalloutAccessoryView = mugIconView
                
                // Add detail button to right callout
                let calloutButton = UIButton(type: .detailDisclosure) as UIButton
                pinView!.rightCalloutAccessoryView = calloutButton
                pinView!.centerOffset = CGPoint(x: 0, y: -18)
            }
            else {
                pinView!.annotation = annotation
            }
            
            return pinView
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
        calloutAccessoryControlTapped control: UIControl) {
            print( NSStringFromClass(type(of: control)).components(separatedBy: ".").last!)
            if control == view.rightCalloutAccessoryView {
                print("Disclosure Pressed! \(view.annotation!.subtitle)")
                
                if let cpa = view.annotation as? ViaggiArtMapPin {
                    print("cpa.imageName = \(cpa.image)")
                    
                    let item : NSDictionary = ["type": cpa.type!, "id" : cpa.id!];
                    EventsHandler.instance.trigger("showItem", information: item)
                    
                }
            }
            
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
       
        //mapView.deselectAnnotation(view.annotation, animated: true)
        
        //print( NSStringFromClass(view.dynamicType).componentsSeparatedByString(".").last!)
        
        if view.annotation is ViaggiArtMapPin{
        let item = view.annotation as! ViaggiArtMapPin
            (view.leftCalloutAccessoryView as! UIImageView).load( item.cover! +  "&size=icon", placeholder:  UIImage(named: "loading"))
        }
    }
    
    /// If user unselects callout annotation view, then remove it.
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
       
    }

    @IBAction func onUserLocation(_ sender: UIButton) {
        
        
        
        _ = vaLoadingActivity.show("Loading".localized, disableUI: true)
        CommonActions.getCurrentLocation(false) { (lat, lng, error) in
            _ = vaLoadingActivity.hide();
            self.waitForFirstLocation = false;
            self.mapView.setCenterCoordinate( CLLocationCoordinate2D(latitude: lat, longitude: lng), zoomLevel: 16, animated: true)
            
            
        }
        
        
    }
    
}



extension MapViewController : UISearchBarDelegate {

    func search(searchText: String){
    
        if( searchText.characters.count > 3 ){
            searchDebouncer.run(delay: 0.8) {
                CLGeocoder().geocodeAddressString(searchText) { placemarks, error in
                    
                    if( error == nil ){
                        
                        if let pos = placemarks?[0].location?.coordinate {
                            
                            self.mapView.setCenterCoordinate( pos, zoomLevel: 15, animated: true)
                            
                            
                        }
                        
                        
                        
                    }
                    
                }
            }
        }
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.search( searchText: searchText );
    }


    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        self.search( searchText: searchBar.text! );
        searchBar.resignFirstResponder() //hide keyboard
        
    }
    
}
