//
//  CreditsViewController.swift
//  AltramaApp
//
//  Created by alessandro on 06/06/17.
//  Copyright © 2017 Altrama Italia SRL. All rights reserved.
//


import UIKit

class CreditsViewController: HamburgerMenuViewController, MenuCallableProtocol{
    
    
    
    
    
    @IBOutlet weak var altramacopy: UILabel!
    
    @IBOutlet weak var buttonTerre: UIButton!
    @IBOutlet weak var buttonAltrama: UIButton!
    @IBOutlet weak var version: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            self.version.text = "Versione".localized + version
        }
        
        //sito.addTarget(self, action: #selector(self.setExploreButtonOn), for: UIControlEvents.touchUpInside)
        // Do any additional setup after loading the view.
        //let logoImage = UIImage(named: "logo")
        //self.navigationItem.titleView = UIImageView(image: logoImage);
        //self.navigationController?.navigationBar.tintColor = UIColor.black
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func websit(_ sender: UIButton) {
        if(sender == buttonAltrama ){
            CommonActions.openURL("http://www.altrama.com")
        }else if(sender == buttonTerre){
            CommonActions.openURL("http://www.lamolazza.it")
        }
        
    }
    
    
    
    func loadData( _ data: Any? ) -> Void{
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

