//
//  ViaggiartMapPin.swift
//  ViaggiArt
//
//  Created by nuccio on 20/02/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import FBAnnotationClusteringSwift

open class ViaggiArtMapPin : FBAnnotation  {
        
        var image : String?
        var cover : String?
        var type : String?
        var address : String?
    var subtitle : String?

    var id: Int?
        
}
