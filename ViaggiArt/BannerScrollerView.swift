//
//  BannerScrollerView.swift
//  ViaggiArt
//
//  Created by nuccio on 02/02/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import UIKit

public protocol BannerScrollViewDelegate : NSObjectProtocol {
    
    func onBannerSelected( _ id : Int );
    func prepareBanner( atPosition id:Int , image: UIImageView );
}

class BannerScrollerView: UICollectionView , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private lazy var __once: () = {
            DispatchQueue.main.async(execute: {
            // scroll to the first page, note that this call will trigger scrollViewDidScroll: once and only once
                self.scrollToItem(  at: IndexPath(item: 1, section: 0), at:UICollectionViewScrollPosition.left, animated: false);
            })
        }()
    
    fileprivate var indexPathForDeviceOrientation: IndexPath = IndexPath(item: 0, section: 0);// for move to the right position after orientation
    fileprivate var token: Int = 0;
    fileprivate var receiver: BannerScrollViewDelegate?;
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        
        /*
         
         var layout: UICollectionViewFlowLayout!
         self.layout = UICollectionViewFlowLayout()
         self.layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
         self.layout.minimumLineSpacing = 0;
         self.layout.minimumInteritemSpacing = 0;
         
         */
        self.setup()
    }
    
    var _dataSource: NSArray = [];// data source
    
    
    
    fileprivate var autoscroll = false;
    var timer : Timer = Timer()
    var interval:Double = 5;
    var page = 1;
    
    func startAutoScroll (_ secs:Double){
        if( !autoscroll ){
        autoscroll = true;
            interval = secs;
            self.timer = Timer.scheduledTimer(timeInterval: secs, target: self, selector: #selector(BannerScrollerView.slide), userInfo: nil, repeats: true)
        }
    }
    
    func stopAutoScroll (_ secs:Float){
        if( autoscroll ){
            
            autoscroll = false;
            timer.invalidate()
        
        }
        
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView){
        if(autoscroll){
            timer.invalidate()
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool){
         if(autoscroll){
            self.timer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(BannerScrollerView.slide), userInfo: nil, repeats: true)
        }
    }

    func slide() {
        let initialPinchPoint = CGPoint(x: self.contentOffset.x + self.frame.size.width/2, y: self.frame.size.height/2);
        
        let centerCellIndexPath : IndexPath = self.indexPathForItem(at: initialPinchPoint)!;
        
        
        if( page == _dataSource.count-1 ){
            self.scrollToItem( at: IndexPath(item: 1, section: 0), at: UICollectionViewScrollPosition.left, animated: false);
            page = 2;
        }else {
         page = (centerCellIndexPath as NSIndexPath).item+1;
        }
       
        
        self.scrollToItem( at: IndexPath(item: page, section: 0), at: UICollectionViewScrollPosition.left, animated: true);
        
        
    }
    
    func setup(){
        
        self.isScrollEnabled = true
        self.showsHorizontalScrollIndicator = false;
        self.showsVerticalScrollIndicator = false;
        self.isPagingEnabled = true
        
        self.dataSource = self
        self.delegate = self
        self.register(BannerItemCell.self, forCellWithReuseIdentifier: "BannerItemCell")
        self.backgroundColor = UIColor.white
        
        
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BannerScrollerView.singleTapped(_:)))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        self.addGestureRecognizer(singleTapGestureRecognizer)
    }
    
    func setupData(pages: Int, receiver: BannerScrollViewDelegate ){
        // data source
        if( pages < 1 ){
            return
        }
        // duplicate the last item and put it at first
        // duplicate the first item and put it at last
        let firstItem = 0;
        let lastItem =  pages-1;
        let workingArray : NSMutableArray = [] ;
        
        (0..<pages).forEach { (i) -> () in
            workingArray.add(i)
        }
        
        workingArray.insert( lastItem, at:0);
        workingArray.add ( firstItem );
        _dataSource = workingArray as NSArray;
        
        self.receiver = receiver
        self.reloadData()
        _ = self.__once
        
    }
    
    func singleTapped(_ sender: UITapGestureRecognizer) {
        let indexPath =  self.indexPathForItem( at: sender.location(in: self) );
        
        receiver?.onBannerSelected((_dataSource[(indexPath! as NSIndexPath).item] as AnyObject).intValue);
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.frame.size;
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerItemCell", for: indexPath) as! BannerItemCell
        let id = (_dataSource[(indexPath as NSIndexPath).item] as AnyObject) as! Int

            if( cell.id != id ){
            cell.id = id;
            receiver?.prepareBanner(atPosition: id, image: cell.imageView!)
        }
        return cell as UICollectionViewCell
    }
    
    func willRotateToInterfaceOrientation(
        _ toInterfaceOrientation: UIInterfaceOrientation,
        duration: TimeInterval) {
            indexPathForDeviceOrientation = self.indexPathsForVisibleItems[0];
            self.collectionViewLayout.invalidateLayout();
    }
    
    func didRotateFromInterfaceOrientation( _ fromInterfaceOrientation: UIInterfaceOrientation)-> Void
    {
        self.scrollToItem(at: indexPathForDeviceOrientation, at:UICollectionViewScrollPosition.left, animated: true );
    }
    
    //#pragma mark - <UIScrollViewDelegate>
    var lastContentOffsetX: CGFloat = CGFloat.leastNormalMagnitude;
    func scrollViewDidEndDecelerating( _ scrollView: UIScrollView ) {
        // We can ignore the first time scroll,
        // because it is caused by the call scrollToItemAtIndexPath: in ViewWillAppear
        
        if (CGFloat.leastNormalMagnitude == lastContentOffsetX) {
            lastContentOffsetX = scrollView.contentOffset.x;
            return;
        }
        
        let currentOffsetX = scrollView.contentOffset.x;
        let currentOffsetY = scrollView.contentOffset.y;
        
        let pageWidth = scrollView.frame.size.width;
        let offset = pageWidth * (CGFloat)(_dataSource.count - 2);
        
        // the first page(showing the last item) is visible and user's finger is still scrolling to the right
        if (currentOffsetX < pageWidth && lastContentOffsetX > currentOffsetX) {
            lastContentOffsetX = currentOffsetX + offset;
            scrollView.contentOffset = CGPoint(x: lastContentOffsetX, y: currentOffsetY);
        }
            // the last page (showing the first item) is visible and the user's finger is still scrolling to the left
        else if (currentOffsetX > offset && lastContentOffsetX < currentOffsetX) {
            lastContentOffsetX = currentOffsetX - offset;
            scrollView.contentOffset = CGPoint (x: lastContentOffsetX, y: currentOffsetY);
        } else {
            lastContentOffsetX = currentOffsetX;
        }
    }
    
}
