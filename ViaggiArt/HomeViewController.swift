//
//  ViewController.swift
//  ViaggiArt
//
//  Created by nuccio on 16/01/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import UIKit
import CoreGraphics
import CoreLocation
import SwiftLocation
import JASON

class HomeViewController: HamburgerMenuTableViewController, MenuCallableProtocol
{
    
    var homepage: [ [String: JSON] ] = [ ];
    var isloaded = false
    
    func loadData(_ data: Any?) {
        if( isloaded  ){
            checkExplore();
            self.refresh();
        }
    }
    
    override func viewDidLoad() {
        isloaded = false
        super.viewDidLoad()
        print("homeviewcontroller view did load")

        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 192;
         self.navigationController?.navigationBar.tintColor = UIColor.black
        
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(HomeViewController.refresh), for: .valueChanged)
        
        self.refreshControl = refresher
        tableView.addSubview( self.refreshControl!)
        
        let yourNibName = UINib(nibName: "HomeListView", bundle: nil)
        tableView.register(yourNibName, forCellReuseIdentifier: "HomeListView")
        
        
        self.refresh();
    }
    
    func refresh() {
        
        self.refreshControl?.beginRefreshing();
        CommonActions.getCurrentLocation { (lat, lng, error) in
            self.checkExplore ()
            _ = vaLoadingActivity.show("Loading".localized, disableUI: true)
            RestApiManager.sharedInstance.getHome( lat, lng: lng, onCompletion: { (data: JSON) -> Void in
                
                
                if(data["data"].object != nil ) {
                
                    

                    self.homepage = [];
                    self.homepage.append( ["banners":  data["data"]["banners"] ] )
                    
                    let sections = data["data"]["sections"];
                    EventsHandler.instance.trigger("createMenu", information: sections )
                    sections.forEach({ (section:JSON) -> () in
                        if( section["list"].arrayValue.count > 0){
                            self.homepage.append( ["section":  section ] )
                            
                        }
                    })
                    
                    if( CommonActions.isExploreEnabled ) {
                        
                        
                        self.homepage.append( ["explore": true ] )
                        
                        
                    }
                }
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
                self.isloaded = true
                _ = vaLoadingActivity.hide()
            })
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let item = homepage[(indexPath as NSIndexPath).row].keys.first {
            
            let value = homepage[(indexPath as NSIndexPath).row].values.first
            
            switch item {
                
            case "banners":
                
                let cell:BannerTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Banner", for:indexPath) as? BannerTableViewCell
                
                
                cell?.load( value! )
                return cell!
                ;
                
                
            case "section":
                
                let cell:HomeListView? = tableView.dequeueReusableCell(withIdentifier: "HomeListView", for:indexPath) as? HomeListView
                
                       cell?.loadData(value!)
                
                        cell?.isUserInteractionEnabled = true
                
                return cell!
                
           /* case "explore":
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "Esplorazione", for:indexPath)
                
                checkExplore()
                return cell
                */
            default:
                break;
                
            }
        }
        return UITableViewCell();
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homepage.count
    }
    
}
