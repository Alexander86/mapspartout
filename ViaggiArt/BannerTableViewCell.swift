//
//  BannerTableViewCell.swift
//  ViaggiArt
//
//  Created by nuccio on 03/06/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit
import JASON
import SwiftCarousel

open class BannerTableViewCell: UITableViewCell , SwiftCarouselDelegate  {

    @IBOutlet weak var altezza: NSLayoutConstraint!
    fileprivate var autoscroll = false;
    var timer : Timer = Timer()
    var interval:Double = 5;
    
    func startAutoScroll (_ secs:Double){
        if( !autoscroll ){
            autoscroll = true;
            interval = secs;
            self.timer = Timer.scheduledTimer(timeInterval: secs, target: self, selector: #selector(BannerScrollerView.slide), userInfo: nil, repeats: true)
        }
    }
    
    func stopAutoScroll (){
        if( autoscroll ){
            autoscroll = false;
            timer.invalidate()
        }
    }
    
    
    func slide() {
    
        var id : Int = carouselView!.selectedIndex! + 1;
        
        if( id >= pageImages.count){
            id = 0
        }
        
        carouselView!.selectItem(id, animated: true)
        
    
    }
    
    
    func someAction(_ sender:UITapGestureRecognizer){
        let id : Int = carouselView!.selectedIndex!;
        let item : NSDictionary = ["type": String(self.pageImages[id].type) as Any, "id" : self.pageImages[id].id];
        EventsHandler.instance.trigger("showItem", information: item)
    }
    
    open func willBeginDragging(withOffset offset: CGPoint) -> Void{
    
        stopAutoScroll()
        
    }
    
    
    
    open func didEndDragging(withOffset offset: CGPoint) -> Void{
      startAutoScroll(5)
    }
    
    @IBOutlet weak var carouselView: SwiftCarousel?
   
    var pageImages: [BannerImage] = []

    func load( _ banners: JSON ){
        let screenSize: CGRect = UIScreen.main.bounds
        carouselView!.delegate = self
        
        let width = min(screenSize.width, screenSize.height);
        let height = width * 0.5;
        
        carouselView?.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: height)
        
        self.altezza.constant = height;
        self.setNeedsUpdateConstraints()
        self.setNeedsLayout()
        
        pageImages = [];
        try! carouselView!.itemsFactory(itemsCount: banners.arrayValue.count) { choice in
            
            let item:JSON = banners[choice]
            let slide =
              BannerImage(
                frame: CGRect(x: 0, y: 0, width: width, height: height),
                type: item["type"].string!,
                id: item["id"].int!,
                url: item["image"].string!,time: choice )
            self.pageImages.append(slide);
            return slide;
        }
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            
            
        }else{
            
            carouselView!.resizeType = .withoutResizing(0)
        }
        
         carouselView!.resizeType = .withoutResizing(0)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(BannerTableViewCell.someAction(_:)))
        carouselView!.addGestureRecognizer(gesture)
        
        carouselView!.defaultSelectedIndex = 0
        self.contentView.bringSubview(toFront: carouselView!)
        carouselView?.scrollType = .max(2)
        carouselView?.selectByTapEnabled = false;
        startAutoScroll(15)
    }

}
