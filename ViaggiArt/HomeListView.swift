//
//  HomeListView.swift
//  ViaggiArt
//
//  Created by nuccio on 07/02/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import UIKit
import JASON

public protocol HomeListViewDelegate : NSObjectProtocol {
    
    func showMap( _ type : String );
    func showAll( _ type : String );
    func showItem( _ type : String , id: Int );
    
}

@IBDesignable open class HomeListView: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    class Item{
        var id:Int = 0
        var title: String?
        var type: String?
        var category: String?
        var image: String?
        var wide: Bool?
        var distance:String?
    }
    
    
    // Our custom view from the XIB file
    @IBOutlet var all: UIButton!
    
    @IBOutlet var cat_image: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    
    
    var eventHandler: HomeListViewDelegate?;
    var type:String?
    var link:String?
    var elements: [Item] = [];
    
    @IBAction func onShowAll(_ sender: AnyObject) {
        
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.leftViewController?.showList( fromUrl: NSURL( string: self.link!)!  , showHamburger: false)
        
        
    }
   
           func xibSetup() {
 

    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        //fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        let bundle = Bundle(for: type(of: self))
        
        collectionView.register(UINib(nibName: "HomePageListCellView", bundle: bundle), forCellWithReuseIdentifier: "MyCell")
        collectionView.register(UINib(nibName: "HomePageListCellWideView", bundle: bundle), forCellWithReuseIdentifier: "MyCellWide")
    }

    open func loadData(){
        self.title.text = "prova"
        
        self.all.isEnabled = true;
        self.all.sizeToFit()
    }
   open func loadData( _ data: JSON ) {
    
        self.cat_image.image = UIImage(named: CommonActions.imageCat[CommonActions.categories[data["type"].stringValue]!]! )
        self.cat_image.layer.cornerRadius = 3.0
        self.cat_image.clipsToBounds = true
        
        self.title.text = data["titolo"].stringValue
        self.type = data["type"].string
        self.link = data["link"].string
        self.elements = [];
    
    
        let title = data["label"].stringValue
        self.all.setTitle( title, for: .normal)
        self.all.isEnabled = true;
        self.all.sizeToFit()
   
        if( self.all.title(for: .normal)?.characters.count == 0 ){
    
            self.all.isEnabled = false;
        
        }
    
        let wide = ( self.type == CommonActions.NEWSS || self.type == CommonActions.OTHERS1 || self.type == CommonActions.EATS || self.type == CommonActions.PLACES || self.type == CommonActions.EVENTS);
    
        data["list"].array?.forEach({ (item) -> () in
            
            let itm = Item();
            
            itm.type = (item["type"] as! String);
            itm.wide = wide
            itm.id  = (item["id"] as! Int)
            itm.title  = (item["title"] as! String)
            itm.category = (item["category"] as! String)
            
            if( itm.type == CommonActions.NEWSS ){
            
                itm.distance = "";
                if( item["elapsed_time"] is NSString ){
                    
                        itm.distance = String( item["elapsed_time"] as! NSString )
                    
                }
                
            }
            else{
            
                itm.distance = "";
            if( item["unit"] is NSString ){
                if( item["distance"] is NSString ){
                    itm.distance = String(item["distance"] as! NSString) + " " + String(item["unit"] as! NSString)
                }
                }
            }
            
            let cover = item["cover"] as! NSDictionary
            itm.image = (cover["url"] as! String)
            
            self.elements.append(itm)
        })
    
    
    
        self.bringSubview(toFront: self.collectionView)
        /*
        let itm = Item();
        
        itm.type = self.type;
        itm.wide = false;
        itm.id  = -1
        itm.title  = "More Items".localized
        itm.category = ""
        itm.image = "plus"
        
        self.elements.append(itm) */
    
      collectionView.reloadData();
    }
    
    
    
    open func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return elements.count
    }
    
    open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.elements[(indexPath as NSIndexPath).item].wide == true ? 150 : 100, height: 176);
    }
    
    open func collectionView(_ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {

            return 10;
    }
    
    open func collectionView(_ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAt section: Int) -> UIEdgeInsets {
    
            return UIEdgeInsets(top: 10,left: 10,bottom: 10,right: 10)
    }
    
    open func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       let item = self.elements[(indexPath as NSIndexPath).row]
       let itm : NSDictionary = ["type": item.type! , "id" : item.id, "home" : true];
        EventsHandler.instance.trigger("showItem", information: itm)

    }
    
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = elements[(indexPath as NSIndexPath).item];
        
        if( item.wide == true ){
            
            let cell:HomePageCollectionReusableViewWide = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCellWide", for: indexPath) as! HomePageCollectionReusableViewWide
            
            if(item.id >= 0) {
                cell.distance.text = item.distance
                cell.Image.load(item.image!, placeholder: UIImage(named: "loading"))
            }else{
                cell.distance.text = ""
                cell.Image.image = UIImage(named: "plus")
                cell.title.textAlignment = .center
            }
            
            
            cell.title.text  = item.title
            cell.category.text = item.category
            cell.Image.layer.cornerRadius = 6.0
            cell.Image.clipsToBounds = true
           
            
            return cell
        }else{
            
            let cell:HomePageCollectionReusableView = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath) as! HomePageCollectionReusableView
            
            
            
            cell.title.text  = item.title
            cell.category.text = item.category
            
            
            if(item.id >= 0) {
                cell.distance.text = item.distance
                cell.Image.load(item.image!, placeholder: UIImage(named: "loading") )
            }else{
                cell.distance.text = ""
                cell.Image.image = UIImage(named: "plus")
            }
            
            cell.Image.layer.cornerRadius = 4.0
            cell.Image.clipsToBounds = true
            
            return cell
            
            
        }
    }
    
    
}
