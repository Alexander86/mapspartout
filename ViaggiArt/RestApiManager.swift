//
//  RestApiManager.swift
//  ViaggiArt
//
//  Created by nuccio on 30/01/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit
import SwiftLocation
import JASON

typealias ServiceResponse = (JSON, NSError?) -> Void


class RestApiManager: NSObject {
    
    
    class paginationControl{
        
        var json: JSON
        
        var page: Int
        var count: Int
        var total: Int
        var total_pages: Int
        
        
        func parse(){
            self.page = self.json["links"]["current_page"].intValue
            self.count = self.json["links"]["count"].intValue
            self.total = self.json["links"]["total"].intValue
            self.total_pages = self.json["links"]["total_pages"].intValue
        }
        
        init( json: JSON ){
            self.json = json["meta"]["pagination"]
            page = 0;
            count = 0;
            total = 0;
            total_pages = 0;
            
            self.parse();
        }
        
        func hasNext() -> Bool {
            return self.json["links"]["next"].string != nil
        }
        
        func hasPrevious() -> Bool {
            return self.json["links"]["previous"].string != nil
        }
        
        
        func nextPage( _ getResults: @escaping (_ json: JSON) -> Void ){
            let meta = self.json["links"]["next"].string;
            
            if( meta != nil){
                RestApiManager.sharedInstance.makeHTTPGetRequest(meta!, key: "nextPage", onCompletion: { json, err in
                    
                    getResults(json)
                    
                    self.json = json["meta"]["pagination"]
                    self.parse()
                    return
                })
            }
            else {
                getResults(nil)
            }
        }
        
        func previousPage( _ onCompletion: @escaping (_ json: JSON) -> Void ){
            let meta = json["links"]["next"].string;
            if( meta != nil){
                RestApiManager.sharedInstance.makeHTTPGetRequest(meta!,key: "previousPage",  onCompletion: { json, err in
                    self.json = json["meta"]["pagination"]
                    self.parse()
                    onCompletion(json)
                })
            }
        }
        
    }
    
    
    static let sharedInstance = RestApiManager()
    var cache: Cache<NSData>? = nil
    let baseURL =  "http://terredelsole.altrama.com/api/2.0/"
   // let baseURL = "http://api.viaggiart.com/2.0/"
   // let baseURL = "http://192.168.1.98:8001/api/2.0/"
    static let unit = "km"
    var userAgent = "iPhone - TerreDelSole 2.0"
    
    
    override init() {
        
        do {
            cache = try Cache<NSData>(name: "api")
            
        } catch _ {
            print("Something went wrong :(")
        }
        
        let model = UIDevice.current.localizedModel
        let osversion = UIDevice.current.systemVersion
        let appversion = Bundle.applicationBuildNumber
        
        userAgent = "ViaggiArt Mobile \(appversion) - \(model) \(osversion)"
    }
    
    func paginate( _ json : JSON, onCompletion: @escaping (_ json: JSON) -> Void)-> JSON{
        
        let meta = json["meta"]["pagination"]["links"]["next"].string;
        if( meta != nil){
            
            makeHTTPGetRequest(meta!, key: "paginate",  onCompletion: { json, err in
                onCompletion(self.paginate(json,onCompletion: onCompletion) )
            })
        }
        
        return json;
        
    }
    
    func getHome(_ lat:Double, lng:Double, onCompletion: @escaping (_ json: JSON) -> Void) {
        
        
        
        //Local Request home
      /*
        do {
            if let file = Bundle.main.url(forResource: "home", withExtension: "json", subdirectory: "data") {
                let data = try Data(contentsOf: file)
                // let json = try JSONSerialization.jsonObject(with: data, options: []) as! [[String:Any]]
                
                onCompletion(JSON(data))
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
 
        */
        // HTTP request OK
        
         let pre = Locale.preferredLanguages[0]
         let route = baseURL + "home/?location=\(lat),\(lng)&lang=\(pre)"
         
         makeHTTPGetRequest(route,  key: "home", onCompletion: { json, err in
         onCompletion(json)
         })
        
    }
    
    
    func getCategories( _ onCompletion: @escaping (_ json: JSON) -> Void) {
        let pre = Locale.preferredLanguages[0]
        
        let route = baseURL + "categories/?lang=\(pre)"
        
        makeHTTPGetRequest(route,  key: "categories", onCompletion: { json, err in
            onCompletion( json )
        })
    }
    
    
    
    func getPlaces(_ byRegion:vaRegion, onCompletion: @escaping (_ json: JSON) -> Void) {
        let pre = Locale.preferredLanguages[0]
        
        let route = baseURL + "places/?location=\(byRegion.latitude),\(byRegion.longitude)&bbox=\(byRegion.minLatitude),\(byRegion.minLongitude),\(byRegion.maxLatitude),\(byRegion.maxLongitude)&verbose=false&lang=\(pre)&unit=\(RestApiManager.unit)&v=2.1"
        
        
        makeHTTPGetRequest(route,  key: "places", onCompletion: { json, err in
            onCompletion(self.paginate(json,onCompletion: onCompletion) )
        })
    }
    
    func getPlacesByLocation(_ latitude:Double, longitude:Double, page: Int = 1, limit: Int = 3, onCompletion: @escaping (_ json: JSON) -> Void) {
        let pre = Locale.preferredLanguages[0]
        
        let route = baseURL + "places/?location=\(latitude),\(longitude)&page=\(page)&limit=\(limit)&verbose=false&lang=\(pre)&unit=\(RestApiManager.unit)&v=2.1"
        
        
        makeHTTPGetRequest(route,  key: "placesbylocation", onCompletion: { json, err in
            onCompletion(json)
        })
    }
    
    
    
    
    func getItemsByLocation(_ type: String, latitude:Double, longitude:Double, page: Int = 1, limit: Int = 3, options: Dictionary<String,String>? = nil, onCompletion: @escaping (_ json: JSON) -> Void) {
        
        
        //Local Request home
      /*
        var typelocal : String?
        
        if(type.contains("provapiatti")){
            typelocal = "provapiatti"
        }else{
            typelocal = "listaRist"
        }
        print("type list \(typelocal) and type = \(type)")
        do {
            
            if let file = Bundle.main.url(forResource: typelocal!, withExtension: "json", subdirectory: "data") {
                let data = try Data(contentsOf: file)
                // let json = try JSONSerialization.jsonObject(with: data, options: []) as! [[String:Any]]
                
                onCompletion(JSON(data))
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
 */
        // http request ok
        
         let pre = Locale.preferredLanguages[0]
         var queryString = "";
         
         if( options != nil ){
         queryString = "&" + options!.queryString
         }
         
         let route = "\(type)/?location=\(latitude),\(longitude)&page=\(page)&limit=\(limit)&verbose=false&lang=\(pre)&unit=\(RestApiManager.unit)" + queryString + "&v=2.1"
         
         
         makeHTTPGetRequest(route,  key: "itemsByLocation", onCompletion: { json, err in
         onCompletion(json)
         })
        
    }
    
    
    func getItems(_ type:String, byRegion:vaRegion, onCompletion: @escaping (_ json: JSON) -> Void) {
        let pre = Locale.preferredLanguages[0]
        
        
        
        let route = baseURL + "\(type)/?location=\(byRegion.latitude),\(byRegion.longitude)&bbox=\(byRegion.minLatitude),\(byRegion.minLongitude),\(byRegion.maxLatitude),\(byRegion.maxLongitude)&verbose=false&lang=\(pre)&unit=\(RestApiManager.unit)&v=2.1"
        
        
        makeHTTPGetRequest(route,  key: "items-\(type)", onCompletion: { json, err in
            onCompletion(self.paginate(json,onCompletion: onCompletion) )
        })
    }
    
    func getRelated( _ type: String,  id:Int, page:Int = 1, limit:Int = 3, onCompletion: @escaping (_ json: JSON) -> Void) {
        if validItems.contains(type) {
            let pre = Locale.preferredLanguages[0]
            
            
            
            
            let route = self.baseURL + "related/\(type)/\(id)/?verbose=false&page=\(page)&limit=\(limit)&lang=\(pre)&format=json&section=bottom&unit=\(RestApiManager.unit)&v=2.1"
            self.makeHTTPGetRequest(route,  key: "item", onCompletion: { json, err in
                onCompletion( json )
            })
            
            
            
            
        }
    }
    
    func getRelatedTop( _ type: String,  id:Int, page:Int = 1, limit:Int = 3, onCompletion: @escaping (_ json: JSON) -> Void) {
        if validItems.contains(type) {
            
            //Local Request related top
            /*
            var typelocal : String?
            
            if(type.contains("other1")){
                typelocal = "relatedPiatto"
            }else{
                typelocal = "relatedRist"
            }
            print("type list \(typelocal) and type = \(type)")
            do {
                
                if let file = Bundle.main.url(forResource: typelocal!, withExtension: "json", subdirectory: "data") {
                    let data = try Data(contentsOf: file)
                    // let json = try JSONSerialization.jsonObject(with: data, options: []) as! [[String:Any]]
                    
                    onCompletion(JSON(data))
                } else {
                    print("no file")
                }
            } catch {
                print(error.localizedDescription)
            }

            
            
            // http ok
           
             
             switch (type){
             
             }
 */
            let pre = Locale.preferredLanguages[0]
            
        
            let route = self.baseURL + "related/\(type)/\(id)/?verbose=false&page=\(page)&limit=\(limit)&lang=\(pre)&format=json&section=top&unit=\(RestApiManager.unit)&v=2.1"
            self.makeHTTPGetRequest(route,  key: "item", onCompletion: { json, err in
                onCompletion( json )
            })
            
            
        }
    }
    
    
    
    func getTrip(_ trip_id:String, stop_id:String, onCompletion: @escaping (_ json: JSON) -> Void) {
        let route = baseURL + "mobility/trip/\(trip_id)?stopid=\(stop_id)"
        makeHTTPGetRequest(route,  key: "items-trip-\(trip_id)-stopid-\(stop_id)", onCompletion: { json, err in
            onCompletion(json)
        })
    }
    
    
    
    
    func getEvents(_ byRegion:vaRegion, onCompletion: @escaping (_ json: JSON) -> Void) {
        let pre = Locale.preferredLanguages[0]
        
        let route = baseURL + "events/?location=\(byRegion.latitude),\(byRegion.longitude)&bbox=\(byRegion.minLatitude),\(byRegion.minLongitude),\(byRegion.maxLatitude),\(byRegion.maxLongitude)&verbose=false&lang=\(pre)&unit=\(RestApiManager.unit)&v=2.1"
        
        
        makeHTTPGetRequest(route,  key: "events",  onCompletion: { json, err in
            onCompletion(self.paginate(json,onCompletion: onCompletion) )
        })
    }
    
    
    func getPlacen( _ id:Int, lat:Double, lng:Double, onCompletion: @escaping (_ json: JSON) -> Void) {
        let pre = Locale.preferredLanguages[0]
        
        let route = baseURL + "place/?location=\(lat),\(lng)&verbose=true&lang=\(pre)&unit=\(RestApiManager.unit)&v=2.1"
        
        
        makeHTTPGetRequest(route,  key: "placen", onCompletion: { json, err in
            onCompletion( self.paginate(json,onCompletion: onCompletion) )
        })
    }
    
    
    let validItems: [String] = ["city", "place", "event", "mobility", "eat", "stay", "shop", "play", "news","tips", "offer","other1","dish" ]
    
    func getItem( _ type: String,  id:Int,  onCompletion: @escaping (_ json: JSON) -> Void) {
        if validItems.contains(type) {
            
            
            //Local Request detail
          /*
            var typelocal : String?
            
            if(type.contains("other1")){
                typelocal = "piatto"
            }else{
                typelocal = "Ristorante"
            }
            print("type list \(typelocal) and type = \(type)")
            do {
                
                if let file = Bundle.main.url(forResource: typelocal!, withExtension: "json", subdirectory: "data") {
                    let data = try Data(contentsOf: file)
                    // let json = try JSONSerialization.jsonObject(with: data, options: []) as! [[String:Any]]
                    
                    onCompletion(JSON(data))
                } else {
                    print("no file")
                }
            } catch {
                print(error.localizedDescription)
            }
            
            */
            // http ok
            
            CommonActions.getCurrentLocation(false, onLocation:{ (lat, lng, error) in
                let pre = Locale.preferredLanguages[0]
                if( !error! ) {
                    let route = self.baseURL + "\(type)/\(id)/?location=\(lat),\(lng)&verbose=true&lang=\(pre)&format=json&unit=\(RestApiManager.unit)&v=2.1"
                    self.makeHTTPGetRequest(route,  key: "item", onCompletion: { json, err in
                        onCompletion( self.paginate(json,onCompletion: onCompletion) )
                    })
                    
                }
                else {
                    
                    let route =  self.baseURL + "\(type)/\(id)/?verbose=true&lang=\(pre)&format=json&v=2.1"
                    self.makeHTTPGetRequest(route,  key: "item", onCompletion: { json, err in
                        onCompletion( self.paginate(json,onCompletion: onCompletion) )
                    })
                    
                }
                
            })
            
        }
    }
    
    func getMap( _ lat: Double, lon: Double ) -> String{
        
        let googleKey="AIzaSyCr3XzfKJ8lbqnIQdXPAXJI3Z7cctqIFtM";
        
        return "http://maps.google.com/maps/api/staticmap?center=\(lat),\(lon)&zoom=15&size=500x150&scale=\(UIScreen.main.scale)&key=\(googleKey)";
        
        // return baseURL + "/resources/ios/\(UIScreen.mainScreen().scale)/\(lat)/\(lon)/map.png";
        
    }
    
    func search( _ query: String, lat: Double?, lng: Double?, page:Int? = 1, onCompletion: @escaping (_ json: JSON) -> Void ) -> Void{
        let pre = Locale.preferredLanguages[0]
        
        var route = self.baseURL + "search/?q=\(query.urlEncodedString())&verbose=false&lang=\(pre)&format=json&v=2.1&limit=10&page=\(page!)"
        
        if( lat != nil ){
            route = route + "&location=\(lat!),\(lng!)&unit=\(RestApiManager.unit)&v=2.1"
        }
        print(route)
        self.makeHTTPGetRequest(route,  key: "search", onCompletion: { json, err in
            onCompletion(json)
        })
    }
    
    
    func json_parseData(_ data: Data) -> NSDictionary? {
        do {
            let json: Any = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            print("[JSON] OK!")
            return (json as? NSDictionary)
        } catch _ {
            print("[ERROR] An error has happened with parsing of json data")
            return nil
        }
    }
    
    
    func loadImage( _ path : String ) -> UIImage {
        
        if let object = (cache?.object(forKey: path)) {
            return UIImage(data: object as Data)!
            
        }
        else {
            if let data = try? Data( contentsOf: URL(string: path)! ) {
                cache?.setObject(data as NSData, forKey: path)
                return UIImage(data: data)!
            }
        }
        
        return UIImage(named: "logo")!
    }
    
    var tasks: Dictionary<String,URLSessionDataTask> = [:]
    func makeHTTPGetRequest(_ path: String, key: String, onCompletion: @escaping ServiceResponse) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        if let val = self.tasks[key] {
            val.cancel();
            self.tasks.removeValue(forKey: key)
        }
        
        print( path )
        
        cache?.setObject(forKey: path, cacheBlock: {  (
            success , error) in
            
            
            let request = NSMutableURLRequest(url: URL(string: path)!)
            let session = URLSession.shared
            request.setValue(self.userAgent, forHTTPHeaderField: "User-Agent")
            request.addValue("ios", forHTTPHeaderField: "os")
            request.addValue("\(UIScreen.main.scale)", forHTTPHeaderField: "scale")
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                
                let result:NSData = (data != nil ? data! : Data() ) as NSData
                //self.cache?.setObject(result as NSData, forKey: "key", expires: .seconds(300)) // Cache response for 5 minutes
                
                success( result, .seconds(20))
            })
            
            task.resume()
            self.tasks[key] = task;
            
            // ... or failure(error)
            
            
            
        }, completion: { (object,ok, error) in
            if (object != nil) {
                DispatchQueue.main.async(execute: {
                    print("response from \(key) \n")
                    onCompletion(JSON(object! as Data), error)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                });
            }
            
            
        } )
        
    }
}
