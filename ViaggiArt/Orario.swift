//
//  Orario.swift
//  ViaggiArt
//
//  Created by nuccio on 16/06/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit

class OrarioListItem: UITableViewCell {
    
    
    @IBOutlet weak var short_name: UILabel!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var time: UILabel!
    
    
    
}
