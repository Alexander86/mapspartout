//
//  MultiRelatedTopCellView.swift
//  AltramaApp
//
//  Created by alessandro on 15/05/17.
//  Copyright © 2017 Altrama Italia SRL. All rights reserved.
//

import UIKit

class MultiRelatedTopCellView: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title: UILabel!
}
