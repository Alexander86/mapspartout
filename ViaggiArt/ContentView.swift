//
//  ContentView.swift
//  ViaggiArt
//
//  Created by nuccio on 06/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable open class ContentView: UITableViewCell  {
   
    @IBOutlet var webview: UIWebView?
    @IBOutlet weak var altezza: NSLayoutConstraint!
    
    var tableView: UITableView? = nil
    var detailView: DetailsView? = nil
    var indexPath: IndexPath? = nil
    
    var firstTime = true;
    
    open func configure(_ content: String) {
        self.webview?.delegate = self
        self.webview?.scrollView.isScrollEnabled = false;
        self.webview?.scrollView.bounces = false;
        self.bringSubview( toFront: self.webview! );
        
        let script = "<script>function openItem(id, type){var url = 'viaggiart://openItem?t=' + type + '&id=' + id; document.location.href = url;}function openURL(url){document.location.href = url;}</script>"
        
        self.webview?.loadHTMLString("<!DOCTYPE html><head><meta charset=\"utf-8\"><meta name =\"viewport\" content = \"user-scalable = no\"><style>" +
            "*{ padding:0; margin:0px;}  img{ width: 100%; height:auto; padding: 20px 0px;}" +
            "body{padding: 0.8em 0.8em;" +
            "color: rgba(0, 0, 0, 0.8);" +
            "font-family: Georgia, Cambria, \"Times New Roman\", Times, serif;" +
            "font-size: 18px;" +
            "font-style: normal;" +
            "font-weight: normal;" +
            "hyphens: manual;" +
            "letter-spacing: -0.072px;" +
            "line-height: 28.44px;" +
            "text-rendering: optimizeLegibility;" +
            "text-size-adjust:100%;" +
            "word-break: break-word;" +
            "word-wrap: break-word;" +
            "-webkit-font-smoothing: antialiased;" +
            "text-align: left;" +
            "}</style></head><body>" + content + script + "</body></html>", baseURL: nil);
        
    }
    
    
    deinit {
        self.webview?.delegate = nil;
    }

}

extension ContentView : UIWebViewDelegate{
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        
       
        if request.url?.scheme == "viaggiart" {
        
        
            let urlComponents = URLComponents(url: request.url!, resolvingAgainstBaseURL: false);
            let queryItems = urlComponents?.queryItems
            let action = request.url?.host;
            let type = ((queryItems?.filter({$0.name == "t"}).first)?.value)! as String
            let id = ((queryItems?.filter({$0.name == "id"}).first)?.value)! as String
            
            if( action == "openItem" ){
                
                let itm : NSDictionary = ["type": type , "id" : id, "home" : false];
                EventsHandler.instance.trigger("showItem", information: itm)
            
            }
            return false;
        }

        if ((request.url?.scheme)! == "http" || (request.url?.scheme)! == "https") &&
            (request.url?.absoluteString.contains("search?q="))! &&
            (request.url?.absoluteString.contains("viaggiart.com"))!{
            let urlComponents = URLComponents(url: request.url!, resolvingAgainstBaseURL: false);
            let queryItems = urlComponents?.queryItems
            let q = ((queryItems?.filter({$0.name == "q"}).first)?.value)! as String
            
            let itm : NSDictionary = ["type": "search" , "query" : q, "id" : -2];
            EventsHandler.instance.trigger("showItem", information: itm)
            return false;
        }

        
        
        if request.url?.scheme == "http" || request.url?.scheme == "https" {
            UIApplication.shared.openURL(request.url!)
            return false;
        }
        
        if navigationType == UIWebViewNavigationType.linkClicked {
            UIApplication.shared.openURL(request.url!)
            return false
        }
        
        
        
        
        return true
    }

    public func webViewDidStartLoad(_ webView: UIWebView)
    {
        
    }
    public func webViewDidFinishLoad(_ webView: UIWebView)
    {       
        let contentHeight = CGFloat( Float(webView.stringByEvaluatingJavaScript(
            from: "document.documentElement.scrollHeight")!)! ) ;
                
        if( (self.detailView?.descriptionHeight)! < contentHeight ){
                self.detailView?.descriptionHeight = contentHeight;
                tableView?.reloadData()
        }
    }
}
