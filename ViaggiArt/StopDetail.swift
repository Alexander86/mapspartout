//
//  StopDetail.swift
//  ViaggiArt
//
//  Created by nuccio on 19/06/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit

class StopDetail: UITableViewCell {
    
    
    @IBOutlet weak var stop_image: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var time: UILabel!
    
    
    
}
