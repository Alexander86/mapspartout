//
//  Debounce.swift
//  ViaggiArt
//
//  Created by Stefano Vena on 06/01/17.
//  Copyright © 2017 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


typealias dispatch_cancelable_closure = (_ cancel : Bool) -> ()

typealias voidFunction = ()->()

class DeBouncer {

    
  private var privateClosure: dispatch_cancelable_closure? = nil
    
    
   private func delay(_ time:TimeInterval, closure:@escaping ()->()) ->  dispatch_cancelable_closure? {
    
    func dispatch_later(_ clsr:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: clsr)
    }
    
    var closure: voidFunction? = closure
    var cancelableClosure:dispatch_cancelable_closure?
    
    let delayedClosure:dispatch_cancelable_closure = { cancel in
        if let clsr = closure {
            if (cancel == false) {
                DispatchQueue.main.async(execute: clsr);
            }
        }
        closure = nil
        cancelableClosure = nil
    }
    
    cancelableClosure = delayedClosure
    
    dispatch_later {
        if let delayedClosure = cancelableClosure {
            delayedClosure(false)
        }
    }
    
    return cancelableClosure;
}

    private func cancel_delay(_ closure:dispatch_cancelable_closure?) {
    if closure != nil {
        closure!(true)
    }

    }
    

    func run(delay: Double = 0.4, closure : @escaping voidFunction){
        
            if( self.privateClosure != nil ){
                cancel_delay(self.privateClosure);
                self.privateClosure = nil;
            }
            
            if( self.privateClosure == nil ){
                self.privateClosure = self.delay(0.4) {
                    closure()
                }
            }

        
        
        }
    
    
    

}
