//
//  LeftMenu.swift
//  ViaggiArt
//
//  Created by nuccio on 16/01/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import UIKit
import JASON

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


protocol MenuCallableProtocol {
     func loadData( _ data: Any? ) -> Void
}


class LeftMenu: UITableViewController {
    
    let cellIdentifier = "LeftMenuCell"
    
    
    
    class MenuItem {
        
        static var current: MenuItem? = nil
        static var currentViewController : MenuCallableProtocol? = nil
        var type :String = "";
        var title:String = "";
        var image:String = "";
        var target:String = "";
        var link:String?
        var data:Any? = nil
        var showHamburger: Bool = true
        
        init( withTitle title:String, image:String, target:String , type:String, link:String? = nil, data: Any? = nil, showHamburger: Bool = true ){
            
            self.title = title;
            self.image = image;
            self.target = target;
            self.data = data
            self.link = link;
            self.type = type;
            self.showHamburger = showHamburger
        }
        
        func isSeparator() -> Bool {
            
            return self.type == "sepa";
            
        }
        
        func isVariable() -> Bool{
            return self.type == "var"
        }
        
        func getTitle() -> String {
            
            return isSeparator() ? "-" : self.title.localized;
        }
        
        func getImage() -> UIImage {
            return UIImage(named: isSeparator() ? "logo" : self.image)!;
        }
        
        func switchController(_ closeDrawer: Bool = true, showHamburger: Bool? = nil) -> Void {
            
            var doIshowHamburger = self.showHamburger;
            
            if( showHamburger != nil ){
                doIshowHamburger = showHamburger!
            }
            
            if( doIshowHamburger ){
                
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                
                if( MenuItem.current !== self ){
                    MenuItem.current = self
                    
                    MenuItem.currentViewController = mainStoryboard.instantiateViewController(withIdentifier: self.target) as? MenuCallableProtocol;
                    
                    MenuItem.currentViewController?.loadData(self.data)
                    
                    
                    
                    let nav = UINavigationController(rootViewController: MenuItem.currentViewController as! UIViewController)
                    
                    (MenuItem.currentViewController as! UIViewController).title = self.getTitle()
                    
                    
                    appdelegate.centerNav?.setViewControllers([], animated: false)
                    appdelegate.centerContainer?.centerViewController.dismiss(animated: true, completion: nil)
                    appdelegate.centerContainer?.centerViewController = nav;
                    appdelegate.centerNav? = nav;
                }
                
                
                
                MenuItem.currentViewController?.loadData(self.data)
                
                if( closeDrawer ){
                    appdelegate.closeDrawer();
                }
            }
            else {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = mainStoryboard.instantiateViewController(withIdentifier: self.target) as? MenuCallableProtocol;
                
                var type: [String]? = self.data as? [String]
                
                if( type == nil ){
                    type = []
                }
                
                while( type?.count < 2 ){
                    type?.append("")
                }
                type?.append("hide hamburger")
                
                controller!.loadData(type)
                
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.centerNav?.pushViewController(controller as! UIViewController, animated: true)
                if( closeDrawer ){
                    appdelegate.closeDrawer();
                }
                
            }
        }
        
        
    }
    
    var menu = ["home": MenuItem(withTitle: "Home",              image: "home"     , target: "HomeViewController", type:"fix" ,data: false as AnyObject? ),
                "map": MenuItem(withTitle: "Map",               image: "map"      , target: "MapView" , type:"fix"),
                "sep": MenuItem(withTitle: "-",                 image: ""         , target: "", type:"sepa")]
    var menuOrder = [0:"home",1:"map",2:"sep"]
    
    
    
    //                   pathComponents[3]  perchè la terza componente è il type per terre del sole. per viaggiart 1
    
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        MenuItem.current = self.menu["home"]
        
        EventsHandler.instance.listenTo("createMenu") { (data:Any?) in
            
            self.menu.removeAll()
            self.menu["home"] = MenuItem(withTitle: "Home",              image: "home"     , target: "HomeViewController", type:"fix" ,data: false as AnyObject? )
            self.menu["map"] = MenuItem(withTitle: "Map",               image: "map"      , target: "MapView" , type:"fix")
            self.menu["sep"] = MenuItem(withTitle: "-",                 image: ""         , target: "", type:"sepa")
            
            self.menuOrder.removeAll()
            self.menuOrder[0]="home"
            self.menuOrder[1]="map"
            self.menuOrder[2]="sep"
            var index = 3
            let jsonobjects = data as? JSON
            jsonobjects?.forEach({  (section:JSON!) -> () in
                let key = section["link"].nsURL
                if(key != nil){
                    self.menu[(key?.pathComponents[3])!] = MenuItem(withTitle: section["titolo"].stringValue, image: section["type"].stringValue, target: "EntityListView", type: "var", link: section["link"].stringValue , data: [section["link"].stringValue])
                    self.menuOrder[index] = (key?.pathComponents[3])!
                    index += 1
                }
            })
            
            self.menu["sepa"] = MenuItem(withTitle: "-",                 image: ""         , target: "", type:"sepa")
            self.menu["cred"] = MenuItem(withTitle: "Credits",               image: "settings"      , target: "CreditsViewController" , type:"fix")
            self.menuOrder[index+1]="cred"
            self.menuOrder[index]="sepa"
            self.tableView.reloadData()
        }
    }
    
    internal func showHome(){
        menu["home"]?.data = true;
        menu["home"]?.switchController(false);
        menu["home"]?.data = false;
    }
    
    
    /*internal func showSearch(){
     menu[0][2].switchController(false);
     }
     */
    internal func showList( _ item: String, showHamburger: Bool = true ){
        menu[item]?.switchController(false,showHamburger: showHamburger);
        
        /*switch item{
         case "places":
         menu[0][4].switchController(false,showHamburger: showHamburger);
         break;
         case     "events":
         menu[0][5].switchController(false,showHamburger: showHamburger);
         break;
         case     "offers":
         menu[0][12].switchController(false,showHamburger: showHamburger);
         break;
         case    "tips":
         menu[0][6].switchController(false,showHamburger: showHamburger);
         break;
         case    "news":
         menu[0][7].switchController(false,showHamburger: showHamburger);
         break;
         case   "eat":
         menu[0][8].switchController(false,showHamburger: showHamburger);
         break;
         case   "stay":
         menu[0][9].switchController(false,showHamburger: showHamburger);
         break;
         case   "shop":
         menu[0][10].switchController(false,showHamburger: showHamburger);
         break;
         case  "mobility":
         menu[0][11].switchController(false,showHamburger: showHamburger);
         break;
         
         default:
         break
         }
         */
        
    }
    
    
    internal func showList( fromUrl: NSURL, showHamburger: Bool = true ){
        
        let itemValue : String = (fromUrl.pathComponents![3]) ;
        
        self.showList( itemValue, showHamburger: showHamburger )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        navigationController?.isNavigationBarHidden = true;
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int)->Int {
        return menu.count;
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let id = indexPath.row
        let key = self.menuOrder[id]
        
        if( key! == "sep" || key! == "sepa"){
            return 20.0;
        }
        else {
            
            return 42.0;
        }
        
        
    }
    
    
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 64)) //set these values as necessary
        returnedView.backgroundColor = tableView.backgroundColor?.withAlphaComponent(0.7)
        
        return returnedView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 64: 0
    }
    
    override func tableView(_ tableView: (UITableView!), cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let id = indexPath.row
        let item = menu[self.menuOrder[id]!]
        //  let item = (menu[(indexPath as NSIndexPath).section][(indexPath as NSIndexPath).row]);
        
        if( item?.isSeparator())!{
            
            let cell : UITableViewCell =  tableView.dequeueReusableCell(withIdentifier: "separator") as UITableViewCell!;
            
            return cell;
            
        }else if(item?.isVariable())!{
            let cell : LeftMenuCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! LeftMenuCellTableViewCell;
            cell.titleLabel?.text = item?.getTitle();
            cell.icon?.image = UIImage(named: CommonActions.imageCat[CommonActions.categories[(item?.image)!]!]! )
            //cell.icon?.load((item?.image)!)
            
            //cell.icon?.image = cell.icon?.image!.withRenderingMode(.alwaysTemplate)
            //cell.icon?.tintColor = UIColor.fromHex("#686868")
            
            return cell
        }else{
            let cell : LeftMenuCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! LeftMenuCellTableViewCell;
            cell.titleLabel?.text = item?.getTitle();
            cell.icon?.image = item?.getImage()
            
            cell.icon?.image = cell.icon?.image!.withRenderingMode(.alwaysTemplate)
            cell.icon?.tintColor = UIColor.fromHex("#686868")
            
            return cell
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let item = menu[self.menuOrder[indexPath.row]!]
        item?.switchController();
        //events.trigger("menu_click", information: (menu[indexPath.section][indexPath.row]));
        
        
    }
    
}
