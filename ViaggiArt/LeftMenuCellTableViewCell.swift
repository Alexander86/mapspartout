//
//  LeftMenuCellTableViewCell.swift
//  ViaggiArt
//
//  Created by nuccio on 29/01/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import UIKit

class LeftMenuCellTableViewCell: UITableViewCell {
    
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var icon: UIImageView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
