//
//  HomePageCollectionReusableView.swift
//  ViaggiArt
//
//  Created by nuccio on 08/02/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import UIKit

class HomePageCollectionReusableView: UICollectionViewCell {
    
    
    @IBOutlet var title: UILabel!
    @IBOutlet var category: UILabel!
    @IBOutlet var distance: UILabel!
    @IBOutlet var Image: UIImageView!
    
    
    
}
