//
//  CalloutAnnotationView.swift
//  SwiftMapViewCustomCallout
//
//  Created by Robert Ryan on 6/15/15.
//  Copyright (c) 2015 Robert Ryan. All rights reserved.
//
//  This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
//  http://creativecommons.org/licenses/by-sa/4.0/

import UIKit
import MapKit

/// Annotation view for the callout
///
/// This is the annotation view for the callout annotation.

class CalloutAnnotationView : MKAnnotationView {
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        
        imageView = UIImageView( frame:  CGRect(x: 2, y: 2, width: 98, height: 98) );
        
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)

        // if the annotation is a callout annotation (and that's the only thing that this should ever
        // be used for!), then add an observer for its title.
        
        if let calloutAnnotation = annotation as? CalloutAnnotation {
            calloutAnnotation.underlyingAnnotation.addObserver(self, forKeyPath: "title", options: [], context: nil)
        } else {
            assert(false, "this annotation view class should only be used with CalloutAnnotation objects")
        }
        
        
        
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        imageView = UIImageView( frame:  CGRect(x: 2, y: 2, width: 98, height: 98) );
        super.init(coder: aDecoder)
    }
    
    /*
    override init
    
    override init(frame: CGRect) {
        imageView = UIImageView( frame:  CGRect(x: 2, y: 2, width: 98, height: 98) );
        super.init(frame: frame)
    }
 
 */

    // if we (re)set the annotation, remove old observer for title, if any and add new one
    
    override var annotation: MKAnnotation? {
        willSet {
            if let calloutAnnotation = annotation as? CalloutAnnotation {
                calloutAnnotation.underlyingAnnotation.removeObserver(self, forKeyPath: "title")
            }
        }
        didSet {
            updateCallout()
            if let calloutAnnotation = annotation as? CalloutAnnotation {
                calloutAnnotation.underlyingAnnotation.addObserver(self, forKeyPath: "title", options: [], context: nil)
            }
        }
    }

    // if this gets deallocated, remove any observer of the title
    
    deinit {
        if let calloutAnnotation = annotation as? CalloutAnnotation {
            calloutAnnotation.underlyingAnnotation.removeObserver(self, forKeyPath: "title")
        }
    }
    
    // if the title changes, update the callout accordingly
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        updateCallout()
    }
    
    let bubbleView = BubbleView()           // the view that actually represents the callout bubble
    let label = UILabel()                   // the label we'll add as subview to the bubble's contentView
    let font = UIFont.systemFont(ofSize: 10)  // the font we'll use
    
    var imageView: UIImageView?
    /// Update size and layout of callout view
    
    func updateCallout() {
        if annotation == nil {
            return
        }
        
        let size = CGSize(width: 100,height: 130)
        
        if let string = annotation?.title , string != nil {
            label.text = (annotation?.title)!
        }
        
        imageView!.load( (self.annotation as! CalloutAnnotation).image!)
        
        print("-----------------")
        print((self.annotation as! CalloutAnnotation).image!)
        print("-----------------")
        bubbleView.setContentViewSize(size)
        frame = bubbleView.bounds
        centerOffset = CGPoint(x: 0, y: -130)
    }
    
    /// Perform the initial configuration of the subviews
    
    func configure() {
        backgroundColor = UIColor.clear
        
        addSubview(bubbleView)
        addSubview(imageView!)
        
        
        imageView!.contentMode = .scaleAspectFit
        label.frame = CGRect(x: 2, y: 100, width: 98, height: 30)
        label.textAlignment = .center
        label.font = font
        label.textColor = UIColor.black
        label.lineBreakMode = .byTruncatingTail
        
        bubbleView.bubbleFillColor = UIColor.white
        bubbleView.bubbleStrokeColor = UIColor.lightGray
        bubbleView.contentView.addSubview(label)
        
        updateCallout()
    }
}

