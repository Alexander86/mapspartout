//
//  MapViewUtils.swift
//  ViaggiArt
//
//  Created by nuccio on 08/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import MapKit

public extension MKMapView {
    
    func getNECoordinate( _ mRect :MKMapRect) -> CLLocationCoordinate2D {
        return self.getCoordinateFromMapRectanglePoint( MKMapRectGetMaxX(mRect), y:mRect.origin.y);
    }
    func getNWCoordinate( _ mRect :MKMapRect) -> CLLocationCoordinate2D {
        return self.getCoordinateFromMapRectanglePoint(MKMapRectGetMinX(mRect), y:mRect.origin.y);
    }
    func getSECoordinate( _ mRect :MKMapRect) -> CLLocationCoordinate2D {
        return self.getCoordinateFromMapRectanglePoint(MKMapRectGetMaxX(mRect), y:MKMapRectGetMaxY(mRect));
    }
    func getSWCoordinate( _ mRect :MKMapRect) -> CLLocationCoordinate2D {
        return self.getCoordinateFromMapRectanglePoint(mRect.origin.x, y:MKMapRectGetMaxY(mRect));
    }
    func getCoordinateFromMapRectanglePoint( _ x: Double, y: Double) -> CLLocationCoordinate2D {
        let swMapPoint: MKMapPoint = MKMapPointMake(x, y);
        return MKCoordinateForMapPoint(swMapPoint);
    }
    
    func getBoundingBox() -> [Double] {
        let mRect: MKMapRect = self.visibleMapRect
        let bottomLeft : CLLocationCoordinate2D = self.getSWCoordinate(mRect);
        let topRight   : CLLocationCoordinate2D = self.getNECoordinate(mRect);
        
        return [ bottomLeft.latitude, bottomLeft.longitude,
                 topRight.latitude  , topRight.longitude   ]
    }
    
    
    func getRegion() -> vaRegion {
        let region = vaRegion();
        var mRect: [Double]  = self.getBoundingBox();
        
        region.latitudeDelta = mRect[2] - mRect[0];
        region.longitudeDelta = mRect[3] - mRect[1];
        
        region.latitude  = (mRect[2] + mRect[0]) / 2.0;
        region.longitude = (mRect[3] + mRect[1]) / 2.0;
        
        
        return region
    }
    
    
    static fileprivate let MERCATOR_OFFSET:Double = 268435456.0
    static fileprivate let MERCATOR_RADIUS:Double = 85445659.44705395
    
    
    
    func longitudeToPixelSpaceX( _ longitude: Double) -> Double
    {
        return round(Double(MKMapView.MERCATOR_OFFSET) + Double(MKMapView.MERCATOR_RADIUS) * longitude * M_PI / 180.0);
    }
    
    func latitudeToPixelSpaceY( _ latitude: Double ) -> Double
    {
        return round(MKMapView.MERCATOR_OFFSET - MKMapView.MERCATOR_RADIUS * log((1 + sin(latitude * M_PI / 180.0)) / (1 - sin(latitude * M_PI / 180.0))) / 2.0);
    }
    
    func pixelSpaceXToLongitude(_ pixelX : Double ) -> Double
    {
        return ((round(pixelX) - MKMapView.MERCATOR_OFFSET) / MKMapView.MERCATOR_RADIUS) * 180.0 / M_PI;
    }
    
    func pixelSpaceYToLatitude(_ pixelY: Double ) -> Double
    {
        return (M_PI / 2.0 - 2.0 * atan(exp((round(pixelY) - MKMapView.MERCATOR_OFFSET) / MKMapView.MERCATOR_RADIUS))) * 180.0 / M_PI;
    }
    
    func getZoom() -> Int {
        // function returns current zoom of the map
        var angleCamera = self.camera.heading
        if angleCamera > 270 {
            angleCamera = 360 - angleCamera
        } else if angleCamera > 90 {
            angleCamera = fabs(angleCamera - 180)
        }
        let angleRad = M_PI * angleCamera / 180 // camera heading in radians
        let width = Double(self.frame.size.width)
        let height = Double(self.frame.size.height)
        let heightOffset : Double = 20 // the offset (status bar height) which is taken by MapKit into consideration to calculate visible area height
        // calculating Longitude span corresponding to normal (non-rotated) width
        let spanStraight = width * self.region.span.longitudeDelta / (width * cos(angleRad) + (height - heightOffset) * sin(angleRad))
        return Int(log2(360 * ((width / 256) / spanStraight))) + 1;
    }
    
    func coordinateSpanWithMapView( _ mapView: MKMapView,
                                    centerCoordinate: CLLocationCoordinate2D,
                                    andZoomLevel zoomLevel: Int ) -> MKCoordinateSpan
    {
        // convert center coordiate to pixel space
        let centerPixelX = self.longitudeToPixelSpaceX(centerCoordinate.longitude);
        let centerPixelY = self.latitudeToPixelSpaceY(centerCoordinate.latitude);
        
        // determine the scale value from the zoom level
        let zoomExponent:Double = 20.0 - Double(zoomLevel);
        let zoomScale = pow(2.0, zoomExponent);
        
        // scale the map’s size in pixel space
        let mapSizeInPixels = mapView.bounds.size;
        let scaledMapWidth = Double(mapSizeInPixels.width) * zoomScale;
        let scaledMapHeight = Double(mapSizeInPixels.height) * zoomScale;
        
        // figure out the position of the top-left pixel
        let topLeftPixelX = centerPixelX - (scaledMapWidth / 2);
        let topLeftPixelY = centerPixelY - (scaledMapHeight / 2);
        
        // find delta between left and right longitudes
        let minLng = self.pixelSpaceXToLongitude(topLeftPixelX)
        let maxLng = self.pixelSpaceXToLongitude(topLeftPixelX + scaledMapWidth)
        let longitudeDelta = maxLng - minLng;
        
        // find delta between top and bottom latitudes
        let minLat = self.pixelSpaceYToLatitude(topLeftPixelY)
        let maxLat = self.pixelSpaceYToLatitude(topLeftPixelY + scaledMapHeight)
        let latitudeDelta = -1 * (maxLat - minLat);
        
        // create and return the lat/lng span
        let span = MKCoordinateSpanMake(latitudeDelta, longitudeDelta);
        return span;
    }
    
    func setCenterCoordinate( _ centerCoordinate: CLLocationCoordinate2D,
                              zoomLevel: Int,
                              animated: Bool ) -> Void
    {
        // clamp large numbers to 28
        let zoomLevel = min(zoomLevel, 28);
        
        // use the zoom level to compute the region
        let span = self.coordinateSpanWithMapView( self, centerCoordinate:centerCoordinate, andZoomLevel:zoomLevel);
        let region = MKCoordinateRegionMake(centerCoordinate, span);
        
        // set the region like normal
        self.setRegion(region , animated:animated);
    }
    
    
}

