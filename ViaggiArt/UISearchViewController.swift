//
//  EntityListController.swift
//  ViaggiArt
//
//  Created by nuccio on 12/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import UIKit
import SwiftLocation
import JASON
import SDWebImage
import JTHamburgerButton
import MMDrawerController


class UISearchViewController: UITableViewController, MenuCallableProtocol, UISearchResultsUpdating{
   
    class ItemsClass {
        var title : String?
        var full_address: String?
        var type : String?
        var id: Int?
        var category: String?
        var city: String?
        var image: String?
        var distance: String?
    }
    var filtered:[ItemsClass] = []
   
    var latitude: Double?;
    var longitude: Double?;
    var searchController: UISearchController!
    var pagination: RestApiManager.paginationControl?
    

    var noAvailableResults: Bool = true
    var actInd: UIActivityIndicatorView?;
    var searchQueryText: String = "";
    
    var debouncer : DeBouncer = DeBouncer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logoImage = UIImage(named: "logo")
        self.navigationItem.titleView = UIImageView(image: logoImage);
        self.navigationController?.navigationBar.tintColor = UIColor.black
        
        // Initializing with searchResultsController set to nil means that
        // searchController will use this view controller to display the search results
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        
        // If we are using this same view controller to present the results
        // dimming it out wouldn't make sense. Should probably only set
        // this to yes if using another controller to display the search results.
        searchController.dimsBackgroundDuringPresentation = false
        
        searchController.searchBar.sizeToFit()
        
        
        searchController.searchBar.text = searchQueryText;
        
        let yourNibName = UINib(nibName: "SearchResultItem", bundle: nil)
        tableView.register(yourNibName, forCellReuseIdentifier: "SearchResultItem")
        
        tableView.tableHeaderView = searchController.searchBar

        // Sets this view controller as presenting view controller for the search interface
        definesPresentationContext = true
        
        CommonActions.getCurrentLocation { (lat, lng, error) in
            
            if( error! ){
                self.latitude = nil
                self.longitude = nil
            }
            else
            {
                self.latitude = lat
                self.longitude = lng
            }
        }
        
        infinityScrollView()
        
        
        
        if( searchQueryText != "" ){
        
            self.searchQuery(searchQueryText)
        
        }
        
        /*
        tableView.addInfiniteScrollWithHandler { (scrollView) -> Void in
            let tableView = scrollView as! UITableView

            if(( self.pagination ) != nil) {
                if(( self.pagination?.hasNext() ) != nil) {
                    self.pagination?.nextPage({ (json:JSON?) in
                        if(json != nil){
                        for item in json!{
                            self.appendResults(item)
                        }
                            tableView.reloadData()
                        }
                        tableView.finishInfiniteScroll()
                    })
                }
            }
            
            
            
        }
       */
    }
    
    func infinityScrollView() {
        
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
        _ = bounds.size.height
        
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 44))
        //footerView.backgroundColor=UIColor.greenColor()
        
        actInd = UIActivityIndicatorView()
        actInd!.center = CGPoint(x: width/2 , y: 22)
        actInd!.color = UIColor.red
        
        footerView.addSubview(actInd!)
        
        self.tableView.tableFooterView = footerView;
        
    }

    
    func appendResults( _ json: JSON ){
        let items = json["data"];
        
        for jsonItem in items {
            let resultItem = ItemsClass()
            
            resultItem.title = jsonItem["title"].stringValue
            resultItem.type = jsonItem["type"].stringValue
            resultItem.category = jsonItem["category"].stringValue
            resultItem.id = jsonItem["id"].intValue
            resultItem.city = jsonItem["city"].stringValue
            resultItem.full_address = jsonItem["full_address"].stringValue
            resultItem.distance = jsonItem["distance"].stringValue
            
            if( resultItem.distance != "") {
              resultItem.distance = resultItem.distance! + " " + jsonItem["unit"].stringValue
            }
            
            resultItem.image = jsonItem["cover"]["url"].stringValue
            
            self.filtered.append( resultItem )
        }               
    }
    
    
    func searchQuery( _ searchText: String ){
        self.actInd?.startAnimating();
        RestApiManager.sharedInstance.search(searchText, lat: self.latitude, lng: self.longitude, onCompletion: { (json) in
            
            self.filtered = []
            self.appendResults(json)
            self.noAvailableResults = (self.filtered.count == 0)
            self.pagination = RestApiManager.paginationControl(json: json)
            DispatchQueue.main.async(execute: {
                self.actInd?.stopAnimating();
                self.tableView.reloadData()
            })
        })

    
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
        
            if( searchText.characters.count > 2 ){
            
                self.debouncer.run(delay: 0.4){
                    
                   self.searchQuery(searchText)
            }
    }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return noAvailableResults ? 1 : filtered.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80.0;
        
    }
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height + 240;
        if (endScrolling >= scrollView.contentSize.height){
            self.actInd?.startAnimating();
            pagination?.nextPage({ (json: JSON?) in
                if( json != nil ){
                    self.appendResults(json!)
                }
                DispatchQueue.main.async(execute: {
                    self.actInd?.stopAnimating();
                    self.tableView.reloadData()
                })
            })
            
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        if( !noAvailableResults ){
            
            let elem = self.filtered[(indexPath as NSIndexPath).row]
            let item : NSDictionary = ["type": String(elem.type!) ?? "", "id" : elem.id!];
            EventsHandler.instance.trigger("showItem", information: item)
        
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if( searchController.searchBar.text?.characters.count == 0 && noAvailableResults) {
            
            let cell = UITableViewCell();
            cell.textLabel?.numberOfLines = 0;
            cell.textLabel?.text = "Scegli la tua prossima destinazione, inserisci il nome di una città o di un luogo di interesse storico.".localized
            
            return cell;
        
        }
        
        
        if( noAvailableResults ){
        
            let cell = UITableViewCell();
            
                cell.textLabel?.text = "Non ci sono risultati disponibili".localized
            
            return cell;
        
        }
        else {
            
        var cell: SearchResultItem? = tableView.dequeueReusableCell(withIdentifier: "SearchResultItem") as? SearchResultItem;
        
        if( cell == nil ){
            
            cell = SearchResultItem();
            
        }
        
        cell?.loadImage( filtered[(indexPath as NSIndexPath).row].image! )
        cell?.name.text = filtered[(indexPath as NSIndexPath).row].title
        cell?.subTitle.text = filtered[(indexPath as NSIndexPath).row].category!
        cell?.distance.text = filtered[(indexPath as NSIndexPath).row].distance
        cell?.address.text = filtered[(indexPath as NSIndexPath).row].full_address
        
        cell?.address.padding_left =  cell?.distance.text  != "" ? 8 : 0
        
            return cell!;
        }
    }
    
    
    
    var button: JTHamburgerButton!
    
    internal func checkExplore (){
     
        var buttons : [UIBarButtonItem] = [  ]
        
        if( CommonActions.isExploreEnabled ){
            
            let button = UIButton()
            button.setImage( UIImage(named:"explore_mode"), for: UIControlState() )
            button.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
            button.addTarget(self, action: #selector(self.setExploreButtonOn), for: UIControlEvents.touchUpInside)
            
            
            buttons.append( UIBarButtonItem(customView: button) )
        }
        
        
        
        self.navigationItem.setRightBarButtonItems(buttons, animated: false);
    }
    
    func setExploreButtonOn(_ sender: UIButton?){
        
        
        let alert = UIAlertController(title: "ViaggiArt", message: "you are in  explore mode".localized, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Esci".localized, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) in
            
            
            CommonActions.stopExploreMode();
            self.checkExplore();
            
            }
            ))
        
        alert.addAction(UIAlertAction(title: "Continua".localized, style: UIAlertActionStyle.default, handler:{ (UIAlertAction) in
            
            }
            ))
        
        if let topController = UIApplication.topViewController() {
            
            topController.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    func OnShowMenu(_ sender: JTHamburgerButton) {
        
        
        
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate;
        
        
        appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil);
        
        self.toggle(appDelegate);
        
    }
    
    internal func toggle(_ appDelegate: AppDelegate? = nil){
        
        if( button == nil){
            return
        }
        
        var myAppDelegate = appDelegate;
        
        if(myAppDelegate == nil){
            myAppDelegate = UIApplication.shared.delegate as? AppDelegate;
        }
        
        if(  myAppDelegate!.centerContainer!.visibleLeftDrawerWidth > 0.0){
            button.setCurrentModeWithAnimation(JTHamburgerButtonMode.arrow)
            
        }
        else {
            button.setCurrentModeWithAnimation(JTHamburgerButtonMode.hamburger)
        }
    }
    
    override var shouldAutorotate : Bool {
        return false;
    }


    func loadData(_ data: Any?) {
        if( data == nil ){
        self.button = JTHamburgerButton(frame: CGRect(x: 0, y: 10, width: 32, height: 32));
        self.button.currentMode = JTHamburgerButtonMode.hamburger;
        self.button.lineColor = UIColor.black
        self.button.updateAppearance()
        self.button.addTarget(self, action: #selector(HamburgerMenuViewController.OnShowMenu(_:)), for: UIControlEvents.touchDown )
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button);
        self.navigationController?.navigationBar.tintColor = UIColor.black
        }
        
        let logoImage = UIImage(named: "logo")
        self.navigationController?.hidesBarsOnSwipe = false
        self.navigationItem.titleView = UIImageView(image: logoImage);
        
        checkExplore ()

        if( data != nil ) {
        
            if let query = data as? String {
                self.searchQueryText = query;
            }
            
        }
        
    }
    
    

    
    
   }
