//
//  DetailsSectionHeader.swift
//  ViaggiArt
//
//  Created by nuccio on 08/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import UIKit

class DetailsSectionHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLabel: UILabel!
}