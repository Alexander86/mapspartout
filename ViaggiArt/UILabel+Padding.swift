//
//  UILabel+Padding.swift
//  ViaggiArt
//
//  Created by nuccio on 27/05/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit

class UILabelPad : UILabel {
   
    var padding_left : CGFloat = 8
    var padding_right : CGFloat = 0
    var padding_top: CGFloat = 0
    var padding_bottom : CGFloat = 0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: padding_top, left: padding_left, bottom: padding_bottom, right: padding_right)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }


}
