//
//  DetailView.swift
//  ViaggiArt
//
//  Created by nuccio on 21/02/16.
//  Copyright © 2016 Altrama Italia SRL. All rights reserved.
//

import Foundation
import UIKit
import JASON

class MobilityStopList: UITableViewController, MenuCallableProtocol {
    
    var stops: [[String]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let logoImage = UIImage(named: "logo")
        self.navigationItem.titleView = UIImageView(image: logoImage);
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stops.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:StopDetail? = tableView.dequeueReusableCell(withIdentifier: "StopDetail", for:indexPath) as? StopDetail
        
        cell?.stop_image.image = UIImage( named: self.stops[ (indexPath as NSIndexPath).row ] [0] )
        cell?.name.text = self.stops[ (indexPath as NSIndexPath).row ] [1]
        cell?.time.text = self.stops[ (indexPath as NSIndexPath).row ] [2]
        
        return cell!;
        
    }
    
    func loadData(_ data: Any?) {
        
        let items = data as! JSON;
        
        self.stops = []
        for item: JSON in items
        {
            
            let time = item["stop"]["arrival_time"].stringValue;
            let name = item["stop"]["stop_name"].stringValue;
            let active = item["stop"]["current_stop"].boolValue;
            var e = [ active ? "stop_active" : "stop", name, time  ]
            
            if( self.stops.count == 0 ){
                e[0] = active ? "first_stop_active" : "first_stop"
            }
            
            
            self.stops.append(e)
        }
        
        if( self.stops.count > 0 ){
            if( self.stops [ self.stops.count-1][0]  == "stop_active" ){
                self.stops [ self.stops.count-1][0]  = "last_stop_active"
            }else{
            
                self.stops [ self.stops.count-1][0]  = "last_stop"
            
            }
        }
        
        tableView.reloadData()
        
    }
    
}
